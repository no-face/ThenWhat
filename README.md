# ThenWhat (WIP)

Library for chain asynchronous operations in c++.

## How to Use

> To be done

## Build

1. Install conan dependencies

        conan install
2. Build

        conan build

   or

        ./waf configure build

## License

The project is released under the 'unlicense' license (see UNLICENSE.md).
