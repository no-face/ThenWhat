#pragma once

#include "ThenWhat/Callable.h"

#include <functional>
#include <memory>

namespace ThenWhat{

template<typename Ret, typename ...Args>
class FunctorCallable : public Callable<Ret, Args...>{
public:
    using Functor = std::function<Ret(Args...)>;

public:
    static std::shared_ptr<FunctorCallable<Ret, Args...>>
    build( Functor f){
        return std::make_shared<FunctorCallable<Ret, Args...>>(f);
    }

public:
    FunctorCallable(Functor f)
        : functor(f)
    {}

    bool valid() const override{
        return (bool)functor;
    }
    void reset() override{
        functor = {};
    }
    Ret call(Args... args) override{
        if(valid()){
            return functor(std::forward<Args>(args)...);
        }

        throw std::runtime_error("Functor is invalid");
    }

protected:
    Functor functor;
};

template<typename Ret, typename ...Args>
auto functorToCallable(std::function<Ret(Args...)> functor)
    -> std::shared_ptr<FunctorCallable<Ret, Args...>>
{
    return FunctorCallable<Ret, Args...>::build(functor);
}

template<typename Ret>
auto functorToCallable(std::function<Ret()> functor)
  -> std::shared_ptr<FunctorCallable<Ret>>
{
  return FunctorCallable<Ret>::build(functor);
}

}//namespace
