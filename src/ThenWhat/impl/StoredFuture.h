#pragma once

#include "SimpleFuturable.h"

namespace ThenWhat{

namespace detail {

template<typename T>
class StoredFutureImpl : public SimpleFuturable<T>{
public:
    using super = SimpleFuturable<T>;
    using OnSuccess = typename super::OnSuccess;
    using OnFailure = typename super::OnFailure;
    using OnFinish  = typename super::OnFinish;

    bool failed(ExceptionStorage error) override{
        if(super::failed(error)){
            this->capturedException = error;

            return true;
        }

        return false;
    }


protected:
    bool hasSucceeded() const{
        return this->status() == CallState::Succeed;
    }

    bool hasFailed() const{
        return this->status() == CallState::Failed;
    }

    void onSuccessImpl(OnSuccess onSuccess) override{
        if(hasSucceeded()){
            deliverResult(onSuccess);
        }
        else if(!this->hasFinished()){
            super::onSuccessImpl(onSuccess);
        }
    }

    void onFailureImpl(OnFailure listener) override{
        if(!listener){
            return;
        }

        if(hasFailed()){
            listener->call(capturedException);
        }
        else{
            super::onFailureImpl(listener);
        }
    }

    void onFinishImpl(OnFinish listener) override{
        if(!listener){
            return;
        }

        if(this->hasFinished()){
            listener->call(this->status());
        }
        else{
            super::onFinishImpl(listener);
        }
    }


    virtual void deliverResult(OnSuccess onSuccess)=0;

protected:
    ExceptionStorage capturedException;
};

}//ns

template<typename T>
class StoredFuture : public detail::StoredFutureImpl<T>{
public:
    using super = detail::StoredFutureImpl<T>;
    using OnSuccess = typename super::OnSuccess;

    bool success(T value) override{
        this->storedValue = value;

        return super::success(std::forward<T>(value));
    }

protected:
    void deliverResult(OnSuccess onSuccess) override{
        onSuccess->call(storedValue);
    }

    T storedValue;
};

template<>
class StoredFuture<void> : public detail::StoredFutureImpl<void>{
public:
    using super = detail::StoredFutureImpl<void>;
    using OnSuccess = typename super::OnSuccess;

    bool success() override{
        return super::success();
    }

protected:
    void deliverResult(OnSuccess onSuccess) override{
        if(onSuccess){
            onSuccess->call();
        }
    }
};

}//namespace
