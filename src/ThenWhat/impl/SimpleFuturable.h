#pragma once

#include "BaseFuturable.h"
#include "ThenWhat/ExceptionStorage.h"

#include "ThenWhat/Promise.h"

#include <vector>
#include <atomic>
#include <algorithm>

namespace ThenWhat{

namespace detail {


template<typename T>
class SimpleFuturableImpl : public BaseFuturable<T>{
public:
    using Super = BaseFuturable<T>;

    using OnSuccess = typename Super::OnSuccess;
    using OnFailure = typename Super::OnFailure;
    using OnFinish  = typename Super::OnFinish;
    using Cancellable = CallablePtr<bool>;

public:
    bool isCancellable() const override{
        return _cancellable && _cancellable->valid();
    }

    bool cancel(bool force=false) override{
        bool canCancel = isCancellable() || force == true;
        if(canCancel && tryCancel(force)){
            finish();

            return true;
        }

        return false;
    }

    CallState status() const override{
        return state;
    }

protected:
    void onSuccessImpl(OnSuccess callable) override{
        onSuccessListeners.push_back(callable);
    }
    void onFailureImpl(OnFailure callable) override{
        onFailureListeners.push_back(callable);
    }
    void onFinishImpl(OnFinish callable) override{
        onFinishListeners.push_back(callable);
    }

public:
    void cancellable(Cancellable cancellable){
        this->_cancellable = cancellable;
    }
    void cancellable(std::function<bool()> cancellableFunctor){
        cancellable(functorToCallable(cancellableFunctor));
    }

protected:

    template<typename Listeners, typename ...Args>
    bool functionFinished(CallState finalState, Listeners&& listenersToNotify, Args&&... result){
        if(!changeStateAndNotify(finalState,
                                 std::forward<Listeners>(listenersToNotify),
                                 std::forward<Args>(result)...))
        {
            return false;
        }


        finish();

        return true;
    }

    template<typename Listeners, typename ...Args>
    bool changeStateAndNotify(CallState finalState, Listeners&& listenersToNotify, Args&&... result){
        if(!changeStateTo(finalState)){
            return false;
        }

        notifyListeners(listenersToNotify, std::forward<Args>(result)...);

        return true;
    }


protected:

    bool tryCancel(bool forced){
        auto previousState = this->status();

        if(!changeStateTo(CallState::Cancelled)){
            return false;
        }

        bool cancelled = doCancel(forced);

        if(!cancelled){
            //restore previous state
            this->state = previousState;
        }

        return cancelled;
    }


    virtual bool doCancel(bool forced = false){
        return (isCancellable() && _cancellable && _cancellable->call()) || forced;
    }

    bool changeStateTo(CallState newState){
        CallState expectedPending = CallState::Pending;
        CallState expectedUnknown = CallState::Unknown;

        //Change state to 'newState' if is 'pending' or 'unknown'
        return state.compare_exchange_strong(expectedPending, newState)
            || state.compare_exchange_strong(expectedUnknown, newState);
    }

    template<typename ...Args>
    bool notifySuccess(Args&&... args){
        return this->functionFinished(CallState::Succeed, this->onSuccessListeners, std::forward<Args>(args)...);
    }

    bool notifyFailure(ExceptionStorage error){
        return functionFinished(CallState::Failed, this->onFailureListeners, error);
    }

    void finish(){
        finish(status());
    }

    void finish(CallState state){
        notifyListeners(onFinishListeners, state);

        onSuccessListeners.clear();
        onFailureListeners.clear();
        onFinishListeners.clear();
    }

    template<typename Listener, typename ...Result>
    void notifyListeners(std::vector<Listener> & listeners, Result&&... result){
        std::remove_if(listeners.begin(), listeners.end(),
            [&](Listener & listener){
                if(!listener || !listener->valid()){
                    return false;
                }

                listener->call(std::forward<Result>(result)...);

                return true;
            });
    }

protected:
    std::atomic<CallState> state{CallState::Pending};

    Cancellable _cancellable;

    //TO-DO: implement some kind of syncronization for listeners (they should be thread-safe)
    std::vector<OnSuccess> onSuccessListeners;
    std::vector<OnFailure> onFailureListeners;
    std::vector<OnFinish> onFinishListeners;
};

}//ns


template<typename T>
class SimpleFuturable : public detail::SimpleFuturableImpl<T>, public virtual Promise<T>{
public:
    bool success(T value) override{
        return this->notifySuccess(std::forward<T>(value));
    }

    bool failed(ExceptionStorage error) override{
        return this->notifyFailure(error);
    }
};


template<>
class SimpleFuturable<void> : public detail::SimpleFuturableImpl<void>, public virtual Promise<void>{
public:

    bool success() override{
        return this->notifySuccess();
    }

    bool failed(ExceptionStorage error) override{
        return this->notifyFailure(error);
    }
};

}//namespace
