#pragma once

#include "BaseFuturable.h"

#include "ThenWhat/ExceptionStorage.h"

namespace ThenWhat{

namespace detail {

    template<typename Value>
    struct Storage{
        template<typename = typename std::enable_if<std::is_default_constructible<Value>::value>::type>
        Storage()
        {}

        Storage(Value value)
            : value(std::move(value))
        {}

        Value value;
    };

    template<>
    struct Storage<void>{
        Storage()
        {}
    };

}//namespace

template<typename T>
class FutureDone : public BaseFuturable<T>{
public:
    using Super = BaseFuturable<T>;

    using OnSuccess = typename Super::OnSuccess;
    using OnFailure = typename Super::OnFailure;
    using OnFinish  = typename Super::OnFinish;

public:
    template<typename X>
    using can_be_omitted = typename std::conditional<
                                std::is_void<X>::value || std::is_default_constructible<X>::value,
                                std::true_type, std::false_type>::type;

public:

    template<typename ...Args>
    static std::shared_ptr<FutureDone<T>> buildPtr(Args&&... args){
        return std::make_shared<FutureDone<T>>(std::forward<Args>(args)...);
    }

public:

    template<typename Result=T,
             typename = typename std::enable_if<can_be_omitted<Result>::value>::type>
    FutureDone()
        : FutureDone((std::is_void<Result>::value ? CallState::Succeed : CallState::Cancelled))
    {}

    template<typename Result=T,
             typename = typename std::enable_if<can_be_omitted<Result>::value>::type>
    FutureDone(CallState state)
        : state(state)
    {}

    template<typename Result,
             typename = typename std::enable_if<
                            ! std::is_void<Result>::value
                            && std::is_convertible<Result, T>::value
                        >::type>
    FutureDone(Result&& value)
        : storedValue(std::forward<Result>(value))
        , state(CallState::Succeed)
    {}

    FutureDone(ExceptionStorage error)
        : error(error)
        , state(CallState::Failed)
    {}

public:
    bool isCancellable() const override{
        //Cannot be cancelled because it is already finished
        return false;
    }

    bool cancel(bool force=false) override{
        //Cannot be cancelled because it is already finished
        return false;
    }

    CallState status() const override{
        return state;
    }

    using Super::hasFinished;

protected:
    void onSuccessImpl(OnSuccess callable) override{
        if(succeeded() && isValid(callable)){
            deliverResult(callable);
        }
    }
    void onFailureImpl(OnFailure callable) override{
        if(failed() && isValid(callable)){
            callable->call(this->error);
        }
    }
    void onFinishImpl(OnFinish callable) override{
        if(hasFinished() && isValid(callable)){
            callable->call(status());
        }
    }

protected:
    bool succeeded() const{
        return status() == CallState::Succeed;
    }
    bool failed() const{
        return status() == CallState::Failed;
    }

    void deliverResult(CallablePtr<void> & callable){
        callable->call();
    }

    template<typename CallableT,
             typename = typename std::enable_if<std::is_convertible<CallableT, OnSuccess>::value>::type>
    void deliverResult(CallableT & callable){
        callable->call(storedValue.value);
    }


    template<typename Ret, typename ...Args>
    bool isValid(const CallablePtr<Ret, Args...> & callable) const{
        return callable && callable->valid();
    }

protected:
    detail::Storage<T> storedValue;
    ExceptionStorage error;

    CallState state{CallState::Pending};
};

}//namespace
