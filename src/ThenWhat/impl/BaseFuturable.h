#pragma once

#include "ThenWhat/Futurable.h"

#include "FunctorCallable.h"

namespace ThenWhat{

template<typename T>
class BaseFuturable : public virtual Futurable<T>{
public:
    using super = Futurable<T>;

    using typename super::OnSuccess;
    using typename super::OnFailure;
    using typename super::OnFinish;

    using OnSuccessFunction = typename super::OnSuccessFunction;

public:

    void onSuccess(OnSuccess callable) override{ onSuccessImpl(callable);}
    void onFailure(OnFailure callable) override{ onFailureImpl(callable);}
    void onFinish (OnFinish  callable) override{ onFinishImpl (callable);}

    void onSuccess(OnSuccessFunction resultFunctor) override{
        onSuccessImpl(functorToCallable(resultFunctor));
    }
    void onFailure(std::function<void(const ExceptionStorage &)> errFunctor) override{
        onFailureImpl(functorToCallable(errFunctor));
    }

    void onFinish(std::function<void(CallState)> finishFunctor) override{
        onFinishImpl(functorToCallable(finishFunctor));
    }

protected:

    virtual void onSuccessImpl(OnSuccess callable)=0;
    virtual void onFailureImpl(OnFailure callable)=0;
    virtual void onFinishImpl(OnFinish callable)=0;
};

}//namespace
