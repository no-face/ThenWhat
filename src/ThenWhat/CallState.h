#pragma once

namespace ThenWhat{

enum class CallState{
    Unknown=0,
    Pending,
    Succeed,
    Failed,
    Cancelled
};

}//namespace
