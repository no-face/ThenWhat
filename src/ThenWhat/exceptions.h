#pragma once

#include <commonex/Exception.hpp>

namespace ThenWhat{

COMMONEX_CLASS(NullDelegateException);

}//namespace
