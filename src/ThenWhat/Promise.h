#pragma once

#include "Futurable.h"

namespace ThenWhat{

template<typename T>
class Promise : public virtual Futurable<T>{
public:
    virtual bool success(T value)=0;

    virtual bool failed(ExceptionStorage error)=0;
};

template<>
class Promise<void> : public virtual Futurable<void>{
public:
    virtual bool success()=0;

    virtual bool failed(ExceptionStorage error)=0;
};


}//namespace
