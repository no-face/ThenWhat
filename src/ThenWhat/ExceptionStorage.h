#pragma once

#include "detail/CatchChain.h"

namespace ThenWhat{

class ExceptionStorage{
public:
    static ExceptionStorage capture(){
        return ExceptionStorage(std::current_exception());
    }

    ExceptionStorage()
        : err(nullptr)
    {}

    template<typename T,
             typename U = typename std::enable_if<is_exception<T>::value>::type>
    ExceptionStorage(const T & ex)
        : ExceptionStorage(std::make_exception_ptr(ex))
    {}
    ExceptionStorage(std::exception_ptr ex)
        : err(ex)
    {}

    bool operator==(const ExceptionStorage & other) const{
        return err == other.err;
    }

    operator bool() const{ return !empty();}
    bool    empty() const{ return !err;}

    std::string getMessage() const{
        std::string msg;
        tryCatch([&msg](const std::exception & e){
            msg = e.what();
        });

        return msg;
    }

    template<typename ExceptionType>
    bool instanceOf() const{
        bool captured=false;
        tryCatch([&captured](const ExceptionType & e){
            captured = true;
        });

        return captured;
    }

    void rethrow() const{
        std::rethrow_exception(err);
    }

    template<typename T,
             typename U = std::enable_if<captor_traits<T>::valid>>
    CatchChain tryCatch(const T & captor) const{
        return tryCatch(captor_traits<T>::toStdFunction(captor));
    }

    template<typename ExceptionType,
             typename U = std::enable_if<is_exception<ExceptionType>::value>>
    CatchChain tryCatch(std::function<void(const ExceptionType &)> captor) const{
        return CatchChain(err).tryCatch(captor);
    }

protected:
    std::exception_ptr err;
};


}//namespace
