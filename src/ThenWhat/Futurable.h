#pragma once

#include "CallState.h"
#include "CallablePtr.h"

#include <functional>

namespace ThenWhat{

class ExceptionStorage;

namespace detail {

template<typename Arg,
         typename = typename std::enable_if<!std::is_same<Arg, void>::value>>
struct _onSuccessFunction{
    using function = std::function<void(Arg)>;
    using callable = Callable<void, Arg>;
    using callable_ptr = CallablePtr<void, Arg>;
};


template<>
struct _onSuccessFunction<void>{
    using function = std::function<void()>;
    using callable = Callable<void>;
    using callable_ptr = CallablePtr<void>;
};

}//namespace

template<typename T>
struct FuturableCallback{
    using OnFailure = CallablePtr<void, const ExceptionStorage &>;
    using OnFinish = CallablePtr<void, CallState>;
    using OnSuccess = typename detail::_onSuccessFunction<T>::callable_ptr;
    using OnSuccessFunction = typename detail::_onSuccessFunction<T>::function;
};

class FuturableCommon{
public:
    using OnFailure = CallablePtr<void, const ExceptionStorage &>;
    using OnFinish = CallablePtr<void, CallState>;

public:
    virtual ~FuturableCommon(){}
    virtual bool cancel(bool force=false)=0;
    virtual bool isCancellable() const=0;
    virtual CallState status() const=0;

    virtual bool hasFinished() const {
        auto state = status();
        return state != CallState::Pending
            && state != CallState::Unknown;
    }

    virtual void onFailure(OnFailure callable)=0;
    virtual void onFinish(OnFinish callable)=0;

    virtual void onFailure(std::function<void(const ExceptionStorage &)> errFunctor)=0;
    virtual void onFinish(std::function<void(CallState)> finishFunctor)=0;
};


template<typename T>
class Futurable : public FuturableCommon{
public:
    using OnSuccess = typename detail::_onSuccessFunction<T>::callable_ptr;
    using OnSuccessFunction = typename detail::_onSuccessFunction<T>::function;

public:
    virtual void onSuccess(OnSuccess callable)=0;
    virtual void onSuccess(OnSuccessFunction resultFunctor)=0;
};

}//namespace
