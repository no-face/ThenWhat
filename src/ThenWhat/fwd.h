#pragma once

/**
    Minimal headers.

    Use this to reduce the included headers when definitions
    are not needed.
*/

#include "Futurable.h"
#include "Callable.h"
#include "CallablePtr.h"
