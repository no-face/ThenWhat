#pragma once

#include "detail/ContinuationState.h"
#include "detail/ContinueConnector.h"

#include "detail/bind.h"
#include "detail/enable_on_void.h"
#include "detail/Ptr.h"

namespace ThenWhat{

template<typename T=void>
class Continuation{
public:
    using Self = Continuation<T>;
    using StatePtr = Ptr<detail::ContinuationState>;

public:

    Continuation()
        : Continuation(std::make_shared<FutureDone<T>>())
    {}

    template<typename Value=T,
             typename = typename disable_on_void<Value>::type,
             typename = typename std::enable_if<std::is_convertible<Value, T>::value>::type>
    Continuation(Value&& value)
        : Continuation(std::make_shared<FutureDone<Value>>(value))
    {}

    Continuation(Ptr<Futurable<T>> f, StatePtr continuationState = nullptr)
        : futurable(f)
        , state(continuationState)
    {
        if(!futurable){
            throw std::invalid_argument("Futurable pointer is null");
        }

        if(! state){
            state = std::make_shared<detail::ContinuationState>();
        }

        state->add(futurable);

        auto weakStorage = toWeak(state);
        auto weakHandle = toWeak(futurable);

        futurable->onFinish([weakStorage, weakHandle](CallState){
            auto s = weakStorage.lock();
            auto h = weakHandle.lock();
            if(s and h){
                s->remove(h);
            }
        });
    }

    void interrupt() const{

        for(detail::ContinuationState::HandlePtr handle : *state){
            if(!handle){
                continue; //Should throw?
            }

            if(!handle->hasFinished()){
                handle->cancel(/* force = */ true);
            }
        }
    }

    CallState status() const{
        return futurable->status();
    }

public:
    template<typename Functor>
    Continuation & onError(Functor f){
        if(futurable){
            futurable->onFailure(f);
        }
        //Throw if not valid?

        return *this;
    }

    template<typename Functor>
    Continuation & onFinish(Functor f){
        if(futurable){
            futurable->onFinish(f);
        }
        //Throw if not valid?

        return *this;
    }

public:
    template<typename Functor,
             typename Return = typename async_callable<Functor>::return_type
             >
    Continuation<Return> then(Functor&& f){
        using Connector = ContinueConnector<T, Return>;

        auto next = std::make_shared<Connector>(std::forward<Functor>(f));

        /* TO-DO: should throw if asyncHandle is null, because the next function
         * will never be called. */
        if(futurable){
            futurable->onSuccess(makeOnSuccessFunctor(next));
            futurable->onFailure(bindMethod(next, &Connector::failed));
            futurable->onFinish([next](CallState state){
                if(next && state == CallState::Cancelled){
                    //cancel next
                    next->cancel(/* force = */ true);
                }
            });
        }

        return Continuation<Return>(next, state);
    }

protected:
    template<typename Ret, typename Arg, typename = typename disable_on_void<Arg>::type>
    std::function<void(Arg)> makeOnSuccessFunctor(Ptr<ContinueConnector<Arg, Ret>> connector){
        return [connector](Arg a){
            connector->start(std::forward<Arg>(a));
        };
    }

    template<typename Ret>
    std::function<void()> makeOnSuccessFunctor(Ptr<ContinueConnector<void, Ret>> connector){
        return [connector](){
            connector->start();
        };
    }

protected:
    Ptr<Futurable<T>> futurable;
    StatePtr state;
};

}//namespace
