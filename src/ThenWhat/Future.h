#pragma once

#include "FuturablePtr.h"
#include "exceptions.h"

#include "impl/BaseFuturable.h"

namespace ThenWhat{

template<typename T>
class Future : public BaseFuturable<T>{
public:
    using super = BaseFuturable<T>;

    using typename super::OnSuccess;
    using typename super::OnFailure;
    using typename super::OnFinish;

public:
    Future(FuturablePtr<T> futurable=nullptr)
        : delegate(futurable)
    {}

    bool valid() const{
        return delegate != nullptr;
    }

    operator bool() const{
        return valid();
    }

    FuturablePtr<T> getFuturable() const{
        return delegate;
    }

    operator FuturablePtr<T> () const{
        return getFuturable();
    }

public://Futurable interface
    bool cancel(bool force=false) override{
        checkDelegate();
        return delegate->cancel(force);
    }

    bool isCancellable() const override{
        checkDelegate();
        return delegate->isCancellable();
    }
    CallState status() const override{
        checkDelegate();
        return delegate->status();
    }

    bool hasFinished() const override{
        checkDelegate();
        return delegate->hasFinished();
    }

protected:

    void onSuccessImpl(OnSuccess callable) override{
        checkDelegate();
        delegate->onSuccess(callable);
    }
    void onFailureImpl(OnFailure callable) override{
        checkDelegate();
        delegate->onFailure(callable);
    }
    void onFinishImpl (OnFinish callable ) override{
        checkDelegate();
        delegate->onFinish(callable);
    }

protected:
    void checkDelegate() const{
        if(delegate == nullptr){
            throw NullDelegateException(__FILE__ ": delegate is null");
        }
    }

protected:
    FuturablePtr<T> delegate;
};

}//namespace
