#pragma once

#include <type_traits>

namespace ThenWhat{

template<typename T>
using enable_on_void = std::enable_if<std::is_void<T>::value>;

template<typename T>
using disable_on_void = std::enable_if<std::is_void<T>::value == false>;

}//namespace
