#pragma once

#include "asynccallable_traits.h"

#include "ThenWhat/impl/FutureDone.h"

#include <functional>

namespace ThenWhat{

namespace detail {

template<typename Ret, typename ...Args>
struct FunctorTypes{
    using HandlePtr    = FuturablePtr<Ret>;
    using FuncType     = Ret(Args...);
    using Functor      = std::function<Ret(Args...)>;
    using AsyncFunctor = std::function<HandlePtr(Args...)>;
};

template<typename Ret>
struct FunctorTypes<Ret, void> : public FunctorTypes<Ret>
{};

template<typename Ret, typename ...Args>
using FunctionType = typename FunctorTypes<Ret, Args...>::FuncType;

}//ns


template<typename FunctionType>
class AsyncCallable;

template<typename Ret, typename ...Args>
class AsyncCallable<Ret(Args...)>{
public:
    using Return       = Ret;
    using FunctorTypes = detail::FunctorTypes<Ret, Args...>;
    using HandlePtr    = typename FunctorTypes::HandlePtr;
    using StdFunctor   = typename FunctorTypes::Functor;
    using AsyncFunctor = typename FunctorTypes::AsyncFunctor;

    using Self = AsyncCallable<Ret(Args...)>;

    template<typename X>
    using disable_on_self = std::enable_if<
        !std::is_same<typename std::decay<X>::type, Self>::value
    >;

public:
    AsyncCallable(){}

    /**
      Create AsyncCallable from a functor.

      Functor can have one of these signatures:
        - Ret(Arg)
        - Ret()
        - FuturablePtr<Ret>(Arg)
        - FuturablePtr<Ret>()
    */
    template<typename Functor,

             //Disable when Functor==Self, to allow default copy constructor and move constructor
             typename = typename disable_on_self<Functor>::type
    >
    AsyncCallable(Functor && f)
        : asyncFunctor(convertFunctor(f))
    {}

    FuturablePtr<Ret> call(Args... a){
        if(asyncFunctor){
            return asyncFunctor(std::forward<Args>(a)...);
        }
        return nullptr;
    }

    bool empty() const{
        return !(bool)asyncFunctor;
    }

    operator bool() const{
        return !empty();
    }

    AsyncFunctor getFunctor() const{
        return asyncFunctor;
    }

protected:
    template<typename Functor,
             typename = typename std::enable_if<
                            std::is_same<Ret, typename async_callable<Functor>::future_result_type>::value
                        >::type>
    static AsyncFunctor convertFunctor(Functor&& f){
        return AsyncFunctor(f);
    }

    template<typename Functor,
             typename = typename std::enable_if<
                            !  async_callable<Functor>::is_async
                             && ! std::is_void<Ret>::value
                             && std::is_same<Ret, typename async_callable<Functor>::result_type>::value
                        >::type>
    static AsyncFunctor convertFunctor(Functor&& f, std::nullptr_t ignore=nullptr){
        return [f](Args... a) -> HandlePtr{
            return FutureDone<Ret>::buildPtr(applyArgs(f, std::forward<Args>(a)...));
        };
    }

    template<typename Functor,
             typename = typename std::enable_if<
                            !  async_callable<Functor>::is_async
                            && std::is_void<Ret>::value
                        >::type>
    static AsyncFunctor convertFunctor(Functor&& f, bool ignore={}){
        return [f](Args... a) -> HandlePtr{
            applyArgs(f, std::forward<Args>(a)...);

            return FutureDone<Ret>::buildPtr();
        };
    }

protected:

    template<typename Functor,
             typename = typename std::enable_if<fn_traits::function_traits<Functor>::arity != 0>::type,
             typename Arg1,
             typename ...OtherArgs>
    static auto applyArgs(Functor f, Arg1&& arg1, OtherArgs&&... args)
        -> fn_traits::fn_result_of<Functor>
    {
        return f(std::forward<Arg1>(arg1), std::forward<OtherArgs>(args)...);
    }


    template<typename Functor,
             typename = typename std::enable_if<fn_traits::function_traits<Functor>::arity == 0>::type,
             typename ...FuncArgs>
    static auto applyArgs(Functor f, FuncArgs&&... args)
        -> fn_traits::fn_result_of<Functor>
    {
        return f();
    }

protected:
    AsyncFunctor asyncFunctor;
};

}//namespace
