#pragma once

#include "ThenWhat/Futurable.h"
#include "ThenWhat/detail/Ptr.h"

#include <deque>
#include <algorithm>

namespace ThenWhat {
namespace detail {

class ContinuationState{
public:
    using HandlePtr = Ptr<FuturableCommon>;
    using Handles = std::deque<HandlePtr>;

    using iterator = Handles::iterator;
    using const_iterator = Handles::const_iterator;

public:
    std::size_t size() const{
        return handles.size();
    }

    void add(HandlePtr handle){
        handles.push_back(handle);
    }

    void remove(HandlePtr handle){
        auto iter = std::remove_if(handles.begin(), handles.end(), [&handle](const HandlePtr & otherHandle){
            return handle == otherHandle;
        });

        handles.erase(iter, handles.end());
    }

    iterator begin(){ return handles.begin();}
    iterator end()  { return handles.end();}

    const_iterator begin() const{ return handles.begin();}
    const_iterator end()   const{ return handles.end();}

protected:
    Handles handles;
};

}//namespace
}//namespace
