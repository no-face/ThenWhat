#pragma once

#include "AsyncCallable.h"

#include "ThenWhat/impl/StoredFuture.h"
#include "ThenWhat/impl/FunctorCallable.h"

#include "bind.h"

namespace ThenWhat{

/**
    Class used to connect the continuation steps.
    That is, it calls the next async function when the first succeed
    or delegate the error and onFinish events.
*/
template<typename In, typename Out>
class ContinueConnector : public StoredFuture<Out>{
public:
    using Self = ContinueConnector<In, Out>;

    using super = StoredFuture<Out>;
    using OnSuccess = typename super::OnSuccess;
    using OnFailure = typename super::OnFailure;
    using OnFinish  = typename super::OnFinish;

    using FuncType     = typename detail::FunctorTypes<Out, In>::FuncType;
    using CallableType = AsyncCallable<FuncType>;

public:

    ContinueConnector()
    {}

    ContinueConnector(CallableType call)
        : asyncCallable(call)
    {
        buildCallables();
    }

    ~ContinueConnector(){
        releaseCallables();
    }

    template<typename ...InArgs>// Use varargs to handle 'void' case
    void start(InArgs&&... args){
        if(asyncCallable){
            delegateHandle = asyncCallable.call(std::forward<InArgs>(args)...);

            registerDelegateListeners();
        }
    }

    bool isCancellable() const override{
        return !this->hasFinished() && (!delegateHandle || delegateHandle->isCancellable());
    }

    /** Called when the previous step has finished. */
    void finished(CallState finishState){
        if(!this->hasFinished()){
            this->changeStateTo(finishState);
        }

        this->finish(this->status());
    }

protected:
    bool doCancel(bool forced=false) override{
        if(delegateHandle){
            return delegateHandle->cancel(forced);
        }
        else if(asyncCallable){
            asyncCallable = {};
        }

        return true;
    }

protected:
    void registerDelegateListeners(){
        if(!delegateHandle){
            return; //maybe should fail/cancel?
        }

        delegateHandle->onSuccess(onSuccessCallable);
        delegateHandle->onFailure(onFailureCallable);
        delegateHandle->onFinish(onFinishCallable);
    }

    void buildCallables(){
        onSuccessCallable = buildSuccessCallable(this);
        onFailureCallable = functorToCallable(bindMethod(this, &Self::onAsyncError));
        onFinishCallable  = functorToCallable(bindMethod(this, &Self::onAsyncFinish));
    }

    void releaseCallables(){
        if(onSuccessCallable){
            onSuccessCallable->reset();
        }
        if(onFailureCallable){
            onFailureCallable->reset();
        }
        if(onFinishCallable){
            onFinishCallable->reset();
        }

        //Clear the pointers
        onSuccessCallable.reset();
        onFailureCallable.reset();
        onFinishCallable.reset();
    }

    void onAsyncError(const ExceptionStorage & e){
        super::failed(e);
    }

    void onAsyncFinish(CallState finishState){
        finished(finishState);
    }

    template<
            typename Arg = Out,
            typename = typename std::enable_if<!std::is_void<Arg>::value>::type>
    CallablePtr<void, Arg> buildSuccessCallable(ContinueConnector<In, Arg> * self){
        return FunctorCallable<void, Arg>::build([self](Arg value){
            self->delegateSuccess(value);
        });
    }

    CallablePtr<void> buildSuccessCallable(ContinueConnector<In, void> * self){
        return FunctorCallable<void>::build([self](){
            self->delegateSuccess();
        });
    }

    template<typename ...OutOrVoid>
    void delegateSuccess(OutOrVoid&&... outOrVoid){
        this->success(std::forward<OutOrVoid>(outOrVoid)...);
    }

protected:
    CallableType asyncCallable;
    FuturablePtr<Out> delegateHandle;

    OnSuccess onSuccessCallable;
    OnFailure onFailureCallable;
    OnFinish onFinishCallable;
};

}//namespace
