#pragma once

#include "ThenWhat/FuturablePtr.h"

#include <function_traits.hpp>

#include <type_traits>
#include <functional>

namespace ThenWhat{


template<typename T>
struct future_results{
    using result_type = std::nullptr_t;
};

template<typename T>
struct future_results<Futurable<T>>{
    using result_type = T;
};


namespace detail {
    template<typename T, template<class> class SubType>
    struct IsFuturable{
        static const constexpr
        bool value = std::is_base_of<Futurable<T>, typename std::decay<SubType<T>>::type>::value;
    };

    template<typename T, template<class> class SubType>
    class FuturableOrNull{
        static const constexpr
        bool is_futurable = IsFuturable<T, SubType>::value;

    public:
        using type = typename std::conditional<is_futurable, Futurable<T>, std::nullptr_t>::type;
    };
}//ns

template<typename T,
         template<class> class FuturableSubType>
struct future_results<FuturableSubType<T>>
    : public future_results<typename detail::
                            FuturableOrNull<T, FuturableSubType>::type>
{};

template<typename T>
struct future_results<std::shared_ptr<T>>
        : public future_results<T>
{
};


template<typename T>
struct async_callable{
    using f_traits = typename fn_traits::function_traits<T>;

    using result_type = typename f_traits::result_type;

    using future_result_type = typename future_results<result_type>::result_type;

    static const constexpr bool is_async = ! std::is_same<std::nullptr_t, future_result_type>::value ;

    using return_type = typename std::conditional<is_async, future_result_type, result_type>::type;
};

template<typename T>
struct is_async_callable{
    static const constexpr bool value =
            ! std::is_same<std::nullptr_t, typename async_callable<T>::future_result_type>::value;
};

}//namespace
