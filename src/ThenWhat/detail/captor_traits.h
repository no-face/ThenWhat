#pragma once

#include <function_traits.hpp>
#include <functional>
#include <type_traits>

namespace ThenWhat{

template<typename T>
using is_exception = typename
                     std::is_base_of<std::exception, T>;

template<typename T>
struct captor_traits{
    using traits = typename fn_traits::function_traits<T>;
    using first_arg = typename
                      std::conditional<traits::arity == 1,
                        typename std::decay<typename traits::template arg<0>::type>::type,
                        void
                      >::type;

public:
    static constexpr const bool valid = is_exception<first_arg>::value;

    using functor = typename std::conditional<!valid, std::nullptr_t, std::function<void(const first_arg &)> >::type;

    template<typename U = typename std::conditional<valid, T, std::nullptr_t>::type >
    static functor toStdFunction(U captor){
        return functor(captor);
    }

    template<typename U >
    static std::nullptr_t toStdFunction(T captor){
        return nullptr;
    }
};

}//namespace
