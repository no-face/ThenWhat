#pragma once

#include <memory>

namespace ThenWhat{

template<typename T>
using Ptr = std::shared_ptr<T>;


template<typename T, typename WeakPtr = std::weak_ptr<T>>
WeakPtr toWeak(Ptr<T> ptr){
    return WeakPtr(ptr);
}

}//namespace
