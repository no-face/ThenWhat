#pragma once

#include "captor_traits.h"

#include <exception>

namespace ThenWhat{

class CatchChain{
public:
    CatchChain()
        : CatchChain(nullptr)
    {}
    CatchChain(std::exception_ptr err)
        : err(err)
    {}

    template<typename T,
             typename U = std::enable_if<captor_traits<T>::valid>>
    const CatchChain & tryCatch(const T & captor) const{
        return tryCatch(captor_traits<T>::toStdFunction(captor));
    }

    template<typename ExceptionType,
             typename U = std::enable_if<is_exception<ExceptionType>::value>>
    const CatchChain & tryCatch(std::function<void(const ExceptionType &)> captor) const{
        if(!err){
            return *this;
        }

        try{
            std::rethrow_exception(err);
        }
        catch(const ExceptionType & e){
            captor(e);

            return solvedChain();
        }
        catch(...){}

        return *this;
    }

protected:

    static const CatchChain & solvedChain(){
        static const CatchChain empty;
        return empty;
    }

protected:
    std::exception_ptr err;
};

}//namespace
