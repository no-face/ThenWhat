#pragma once

namespace ThenWhat{

template<typename T, typename Ret, typename ...Args>
std::function<Ret(Args...)> bindMethod(T * instance, Ret(T::*method)(Args...)){
    return [instance, method](Args... args) ->Ret{
        return (instance->*method)(std::forward<Args>(args)...);
    };
}


template<typename Pointer, typename T, typename Ret, typename ...Args>
std::function<Ret(Args...)>
bindMethod(Pointer ptr, Ret(T::*method)(Args...)){
    return [ptr, method](Args... args) ->Ret{
        auto instance = ptr.get();
        return (instance->*method)(std::forward<Args>(args)...);
    };
}

}//namespace
