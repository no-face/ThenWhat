#pragma once

/**
    Header that includes the library interfaces and implementations.
*/

#include "interfaces.h"

#include "impl/FunctorCallable.h"
#include "impl/SimpleFuturable.h"

#include "continuation.h"
#include "Future.h"
