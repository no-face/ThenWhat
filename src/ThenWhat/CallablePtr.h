#pragma once

#include "Callable.h"

#include <memory>

namespace ThenWhat{

template<typename Ret, typename ...Args>
using CallablePtr = std::shared_ptr<Callable<Ret, Args...>>;

}//namespace
