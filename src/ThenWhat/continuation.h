#pragma once

#include "Continuation.h"

namespace ThenWhat{

Continuation<> startContinuation(){
    return Continuation<>();
}

template<typename Functor,
         typename Return = typename async_callable<Functor>::return_type
         >
Continuation<Return> startContinuation(Functor&& functor){
    return Continuation<>()
                .then(functor);
}


/** Create a continuation for a futurable pointer.
*/
template<template<class> class FuturableType,
         typename T,
         typename = typename std::enable_if<std::is_base_of<Futurable<T>, FuturableType<T>>::value>::type>
Continuation<T> continueFrom(Ptr<FuturableType<T>> f){
    return Continuation<T>(f);
}

/** Create a continuation for a type convertible to Ptr<Futurable<T>>.
*/
template<template<class> class FuturableType,
         typename T,
         typename = typename std::enable_if<std::is_convertible<FuturableType<T>, Ptr<Futurable<T>>>::value>::type>
Continuation<T> continueFrom(FuturableType<T> f){
    return continueFrom(static_cast<Ptr<Futurable<T>>>(f));
}

}//namespace
