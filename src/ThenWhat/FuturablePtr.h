#pragma once

#include "Futurable.h"

#include <memory>

namespace ThenWhat {

template<typename T>
using FuturablePtr = std::shared_ptr<Futurable<T>>;

}//namespace
