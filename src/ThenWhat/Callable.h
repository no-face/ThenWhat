#pragma once

#include <utility>

namespace ThenWhat{

template<typename Ret, typename ...Args>
class Callable{
public:
    virtual ~Callable(){}
    virtual bool valid() const=0;
    virtual void reset()=0;
    virtual Ret call(Args... args)=0;

public:
    operator bool() const{
        return valid();
    }

    Ret operator()(Args... args){
        return call(std::forward<Args>(args)...);
    }
};

template<typename Ret>
class Callable<Ret, void> : public Callable<Ret>
{};

}//namespace
