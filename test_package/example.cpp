#include "ThenWhat/all.h"

#include <vector>
#include <iostream>
#include <random>
#include <sstream>
#include <future>
#include <chrono>

using namespace ThenWhat;
using namespace std;

template<
        typename T,
        typename Functor,
        typename FuturablePtr = Ptr<Futurable<T>>
        >
FuturablePtr runAsync(Functor&& callable){
    auto promise = std::make_shared<StoredFuture<T>>();

    std::async(std::launch::async, [promise, callable](){
        callable(promise);
    });

    return promise;
}

int randomValue(int min, int max);

FuturablePtr<int>               generateInt();
FuturablePtr<std::vector<int>>  generateVector(int lenght);
FuturablePtr<std::string>       vectorToString(const std::vector<int> & result);

class Wait{
public:

    Wait()
    {
        reset();
    }


    bool wait(long timeoutMillis){
        auto state = f.wait_for(std::chrono::milliseconds(timeoutMillis));

        return state == std::future_status::ready;
    }

    void notifyFinished(){
        promise.set_value(true);
    }

    void reset(){
        promise = std::promise<bool>();
        f = promise.get_future();
    }

protected:
    std::promise<bool> promise;
    std::future<bool> f;
};


int main(){
    Wait wait;

    auto continuation =
            startContinuation(&generateInt)
            .then(&generateVector)
            .then(&vectorToString)
            .then([](const std::string & result){
                cout << "Received value: \"" << result << "\"" << endl;
            })
            .onError([](const ExceptionStorage & err){
                cout << "Error!" << endl;
            })
            .onFinish([&wait](CallState state){
                cout << "On finish!" << endl;
                wait.notifyFinished();
            });

    if(!wait.wait(500)){
        cout << "Wait timed out" << endl;
    }

    wait.reset();

    cout << "Continuing from future" << endl;

    Future<int> f = generateInt();
    auto continueFromFuture = continueFrom(f)
            .then([](int v){
                cout << "Continuation from future received: " << v << endl;
            })
            .onFinish([&wait](CallState state){
                cout << "Finished continuation from future" << endl;
                wait.notifyFinished();
            });

    if(!wait.wait(500)){
        cout << "Continuation from future timed out" << endl;
    }
    cout << "Bye" << endl;

    return 0;
}

/*********************************************************************************************/

FuturablePtr<int> generateInt(){
    return runAsync<int>([](Ptr<Promise<int>> promise){
        auto randValue = randomValue(1, 10);

        cout << "asyncOp1: Generated random value: " << randValue << endl;

        promise->success(randValue);
    });
}

FuturablePtr<std::vector<int>> generateVector(int lenght){
    return runAsync<std::vector<int>>([lenght](Ptr<Promise<std::vector<int>>> promise){
        cout << "asyncOp2: Generating vector with " << lenght << " values"<< endl;

        std::vector<int> values;
        values.reserve(lenght);

        for(auto i=0; i < lenght; ++i){
            values.push_back(randomValue(1,100));
        }

        promise->success(values);
    });
}

template<typename T>
std::string toString(const std::vector<T> & result){
    std::stringstream out;
    out << "[";

    for (size_t i = 0; i < result.size(); ++i) {
        if(i != 0){
            out << ", ";
        }

        out << result[i];
    }

    out << "]";

    return out.str();
}

FuturablePtr<std::string> vectorToString(const std::vector<int> & result){
    return runAsync<string>([result](Ptr<Promise<std::string>> promise){
        cout << "asyncOp3: Converting vector to string" << endl;

        promise->success(toString(result));
    });
}

int randomValue(int min, int max){
    static std::mt19937 rng{std::random_device()()};

    std::uniform_int_distribution<int> distrib(min,max);

    return distrib(rng);
}
