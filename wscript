#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import path

from waflib.Configure import conf

def load_tools(ctx):
    ctx.load('compiler_cxx')
    ctx.load('gnu_dirs')

def options(ctx):
    #Load options from waf tools (c++ compiler)
    # Execute ./waf --help to see current options
    load_tools(ctx)

    #You can add your own options also

    ctx.add_option('--debug', action='store_true', default=True, dest='debug', help='Do debug build')
    ctx.add_option('--release', action='store_false', dest='debug', help='Do release build')

    ctx.add_option('--build-tests', action='store', dest='build_tests', default=True, help='Build tests')
    ctx.add_option('--build-examples', action='store', dest='build_examples', default=False, help='Build examples (test_package)')

def configure(ctx):
    load_tools(ctx)

    # Load references to conan dependencies (created after 'conan install').
    # Searches 'conanbuildinfo_waf' in current directory or above build dir,
    # to allow conan builds
    ctx.load('conanbuildinfo_waf', tooldir=[".", path.join(ctx.bldnode.abspath(), "..")]);

    ctx.env.test_deps = ['Catch', 'FakeIt', 'ubehavior']
    ctx.env.main_deps = [d for d in ctx.env.conan_dependencies if d not in ctx.env.test_deps]


    # Allows debug build
    if ctx.options.debug:
        ctx.env['CXXFLAGS'] += ['-g']
        ctx.env['CFLAGS'] += ['-g']

    ctx.env.CXXFLAGS += ['-std=c++11', '-Wall']

    # Option can be set as string, so we convert it to boolean
    ctx.env.build_tests    = (ctx.options.build_tests    == True or ctx.options.build_tests    == 'True')
    ctx.env.build_examples = (ctx.options.build_examples == True or ctx.options.build_examples == 'True')

    if ctx.env.build_tests:
        ctx.recurse('test')

def build(bld):
    bld.recurse('src')

    if bld.env.build_tests:
        bld.recurse('test')

    if bld.env.build_examples:
        bld.program(
            target    = 'example',
            source    = "test_package/example.cpp",
            linkflags = ["-pthread"],
            use       = ["ThenWhat"])

################################### Helpers ###########################################
# Methods with '@conf' can be used from a 'ctx' instance. Ex.: ctx.glob(...)

@conf
def glob(ctx, *k, **kw):
    ''' Executes a search for files and/or directories relative to the wscript file.
        Returns a list of the matched waf Nodes (waflib.Node.Node)

        See documentation for 'ant_glob' search at: https://waf.io/apidocs/Node.html?#waflib.Node.Node.ant_glob
    '''

    return ctx.path.ant_glob(*k, **kw)

@conf
def lib(bld, **kw):
    ''' Builds a library (static, dynamic or header-only) and installs
        the generated files and headers.

        If is a header-only library (no sources) only exports the header files.
        If 'bld.env.shared' is True, builds a shared library.
        Otherwise a static library is built.

        Arguments:
            only_objects    : if True, does not link the objects into a library.
            install_headers : explicit what headers should be installed
            headers_dir     : if install_headers is not set, search these directories
                              for '.h' or '.hpp' files [defaults to the value of 'export_includes']
            headers_base    : if set, use this directory as reference to derive the relative path
                                of export headers.
                              Ex.: headers_base = /my/path
                                   header       = /my/path/lib/sample.h
                                   final dir    = /install/path/lib/sample.h

        Other arguments used in 'bld.shlib(...)' or 'bld.stlib(...)' can also be used.
    '''

    if 'install_path' not in kw:
        kw['install_path'] = bld.env.LIBDIR

    _lib_build_objects(bld, kw)

    _lib_build_library(bld, **kw)

    _lib_install_headers(bld, **kw)

def _lib_build_objects(bld, kw):
    # NOTE: keywords (kw) should be passed as dict to allow
    # changing it

    from waflib import Utils, Logs

    source = kw.get('source', [])

    if not source:
        return

    only_objects = kw.get("only_objects", False)

    objects_kw = kw.copy()

    if not only_objects:
        objects_target = kw.get('target') + ".objects"
        objects_kw['target'] = objects_target

    bld.objects(**objects_kw)

    # Changes 'use' to depends on built objects
    kw['use'] = [objects_target] + Utils.to_list(kw.get('use', ""))

def _lib_build_library(bld, **kw):
    from waflib.Tools import c_aliases

    if not kw.get("source", []): #header only library
        bld(**kw)

    elif not kw.get("only_objects", False):
        lib_type = "shlib" if bld.env.shared else "stlib"
        c_aliases.set_features(kw, lib_type)

        # remove source to not build the same sources again
        kw['source']   = []

        # build library
        bld(**kw)

def _lib_install_headers(bld, **kw):
    from waflib import Utils

    includedir = kw.get('install_includedir', bld.env.INCLUDEDIR)

    install_headers = []

    if 'install_headers' in kw:
        install_headers = kw['install_headers']
    else:
        headers_dirs = kw.get('headers_dirs', kw.get('export_includes', []))

        for inc_dir in headers_dirs:

            inc_node = bld.path.make_node(inc_dir)
            incs = inc_node.ant_glob([
                                path.join('**', '*.h'),
                                path.join('**', '*.hpp')])

            install_headers.extend(incs)

    if install_headers:
        headers = Utils.to_list(install_headers)

        headers_base = kw.get("headers_base", None)
        relative_trick = kw.get("install_relative", True)
        bld.install_files(
            includedir,
            headers,
            relative_trick=relative_trick,
            cwd=headers_base)
