#pragma once

#include "ThenWhat/FuturablePtr.h"
#include "ThenWhat/impl/SimpleFuturable.h"

#include <vector>

namespace ThenWhat {

template<typename T>
class FuturableMockup : public SimpleFuturable<T>{
public:
    using Ptr = typename std::shared_ptr<FuturableMockup<T>>;

    static Ptr build(){
        return std::make_shared<FuturableMockup<T>>();
    }

public:
    using super = SimpleFuturable<T>;

    using OnSuccess = typename super::OnSuccess;
    using OnFailure = typename super::OnFailure;
    using OnFinish  = typename super::OnFinish;

public:

    bool isCancellable() const override{
        ++isCancellableCalls;
        return canCancel;
    }
    bool cancel(bool force=false) override{
        ++cancelCalls;

        bool cancelled = (canCancel || (force && forceCancellable)) && super::changeStateTo(CallState::Cancelled);

        if(cancelled){
            super::finish();
        }

        return cancelled;
    }
    CallState status() const override{
        ++statusCalls;
        return super::status();
    }

    bool hasFinished() const override{
        ++hasFinishedCalls;
        auto state = super::status();
        return state != CallState::Pending
            && state != CallState::Unknown;
    }

protected:
    void onSuccessImpl(OnSuccess callable) override{
        ++onSuccessCalls;
        super::onSuccessImpl(callable);
    }
    void onFailureImpl(OnFailure callable) override{
        ++onFailureCalls;
        super::onFailureImpl(callable);
    }
    void onFinishImpl(OnFinish callable) override{
        ++onFinishCalls;
        super::onFinishImpl(callable);
    }

public://expose listeners
    std::vector<OnSuccess> & onSuccessListeners(){
        return super::onSuccessListeners;
    }
    std::vector<OnFailure> & onFailureListeners(){
        return super::onFailureListeners;
    }
    std::vector<OnFinish> & onFinishListeners(){
        return super::onFinishListeners;
    }

public:
    bool canCancel = true, forceCancellable=true;
    mutable std::size_t isCancellableCalls=0, cancelCalls=0, statusCalls=0, hasFinishedCalls=0;
    mutable std::size_t onSuccessCalls=0, onFailureCalls=0, onFinishCalls=0;
};

}//namespace
