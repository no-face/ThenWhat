#pragma once

#include "test.h"

#include "ThenWhat/impl/FunctorCallable.h"

#include <exception>

using namespace ThenWhat;

class FunctorCallableScenarios : public TestFixture<FunctorCallableScenarios>{
public:
    STEP_given_(a_functor_instance)(){
        functor = [this](){
            functorCalled = true;
        };
    }

    STEP_when_(creating_a_simple_callable)(){
        callable = FunctorCallable<void>::build(functor);
        REQUIRE(callable);
    }

    STEP_when_(calling_it)(){
        try{
            callable->call();
        }
        catch(...){
            capturedException = std::current_exception();
        }
    }

    STEP_then_(the_functor_should_have_been_called)(){
        CHECK(functorCalled);
    }

public:

    STEP_given_(a_valid_SimpleCallable)(){
        given().a_functor_instance();
        when().creating_a_simple_callable();

        CHECK(callable->valid());
    }
    STEP_when_(resetting_it)(){
        callable->reset();
    }
    STEP_then_(an_exception_should_have_been_called)(){
        CHECK_FALSE(functorCalled);

        CHECK(capturedException != nullptr);
    }
    STEP_then_(the_callable_should_not_be_valid_anymore)(){
        CHECK_FALSE(callable->valid());
    }

protected:
    std::function<void()> functor;
    bool functorCalled=false;

    std::shared_ptr<Callable<void>> callable;

    std::exception_ptr capturedException;
};
