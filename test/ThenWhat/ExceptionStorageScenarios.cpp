#include "ExceptionStorageScenarios.h"

#define FIXTURE ExceptionStorageScenarios
#define TEST_TAG "[ExceptionStorage]"

/* *********************************************************/

FIXTURE_SCENARIO("[ExceptionStorage] can retrieve the exception message"){
    given().an_exception_with_message("Example");
    when().storing_that_exception();
    then().the_ExceptionStorage_message_should_be("Example");
}

FIXTURE_SCENARIO("[ExceptionStorage] can check the exception type"){
    checkExceptionType<std::runtime_error>(std::runtime_error("Same"), true);
    checkExceptionType<std::exception>(std::runtime_error("Base class"), true);
    checkExceptionType<std::logic_error>(std::runtime_error("Other class"), false);
    checkExceptionType<std::invalid_argument>(std::logic_error("Inverted relation"), false);
}

FIXTURE_SCENARIO("[ExceptionStorage] allows capturing the stored exception if captor matches"){
    given().a_non_empty_ExceptionStorage();
    when().trying_to_catch_the_stored_exception_with_a_compatible_functor();
    then().the_functor_should_have_been_called_with_the_stored_exception();
}

FIXTURE_SCENARIO("[ExceptionStorage] 'captor_traits' can capture a lambda function"){
    auto lambda =[this](const std::runtime_error & e){
        this->capturedException = ExceptionStorage(e);
    };

    using LambdaT = decltype(lambda);

    CHECK(captor_traits<LambdaT>::valid);
    CHECK(captor_traits<LambdaT>::traits::arity == 1);

    using FirstArg       = captor_traits<LambdaT>::first_arg;
    auto isExceptionType = std::is_base_of<std::exception, FirstArg>::value;
    auto isRuntimeError  = std::is_base_of<std::runtime_error, FirstArg>::value;

    CAPTURE(typeid(FirstArg).name());
    CHECK(isExceptionType);
    CHECK(isRuntimeError);
}

FIXTURE_SCENARIO("[ExceptionStorage] can chain captors"){
    given().an_stored_exception();
    when().chaining_captors_to_capture_that_exception();
    then().the_first_matching_captor_should_be_called();
    and_().the_other_captors_should_not_have_been_called();
}
