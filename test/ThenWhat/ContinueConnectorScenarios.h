#pragma once

#include "test.h"

#include "ThenWhat/detail/ContinueConnector.h"

#include "mockups/FuturableMockup.h"

using namespace ThenWhat;

class ContinueConnectorScenarios : public TestFixture<ContinueConnectorScenarios>{
public:

public:
    STEP_given_(an_ContinueConnector_created_from_an_AsyncCallable)(){
        junction = std::make_shared<IntToStringContinuation>([this](int result){
            this->capturedResult = result;

            strHandleMockup = FuturableMockup<std::string>::build();
            return strHandleMockup;
        });
    }

    STEP_when_(it_receives_a_successful_result)(){
        expectedResult = 123456;
        junction->start(expectedResult);
    }

    STEP_then_(the_AsyncCallable_should_be_called_with_the_received_value)(){
        CHECK(capturedResult == expectedResult);
    }

public: //void Input
    STEP_given_(a_valid_void_ContinueConnector)(){
        voidJunction = std::make_shared<VoidToStringJunction>([this](){
            this->functionCalled = true;

            strHandleMockup = FuturableMockup<std::string>::build();
            return strHandleMockup;
        });
    }

    STEP_when_(start_is_called)(){
        voidJunction->start();
    }

    STEP_then_(the_AsyncCallable_should_be_called)(){
        CHECK(functionCalled);
    }

public:
    STEP_given_(a_ContinueConnector_waiting_for_the_AsyncCallable_result)(){
        given().an_ContinueConnector_created_from_an_AsyncCallable();
        when().it_receives_a_successful_result();
    }

    STEP_given_(that_an_OnSuccess_listener_has_been_registered)(){
        junction->onSuccess([this](const std::string & result){
            onSuccessCalled = true;
            this->capturedStrResult = result;
        });
    }

    STEP_given_(that_an_error_listener_has_been_registered)(){
        junction->onFailure([this](const ExceptionStorage & e){
            onFailureCalled = true;
            capturedException = e;
        });
    }

    STEP_given_(that_an_OnFinish_listener_has_been_registered)(){
        junction->onFinish([this](CallState finishState){
            this->capturedFinishState = finishState;
        });
    }

    STEP_when_(the_AsyncCallable_succeeds)(){
        REQUIRE(strHandleMockup);

        expectedStrResult = "Lorem Ipsum dolor sit amet";
        strHandleMockup->success(expectedStrResult);
    }

    STEP_when_(the_AsyncCallable_fails)(){
        strHandleMockup->failed(ExceptionStorage(std::runtime_error("Example error")));
    }

    STEP_when_(the_AsyncCallable_is_cancelled)(){
        strHandleMockup->cancel(true);
    }

    STEP_when_(the_AsyncCallable_is_finished_with_state)(CallState finishState){
        junction->finished(finishState);
    }

    STEP_then_(the_OnSuccess_listener_should_be_called_with_the_result)(){
        CHECK(onSuccessCalled);
        CHECK(capturedStrResult == expectedStrResult);
    }

    STEP_then_(the_error_listener_should_be_called_with_the_error_instance)(){
        CHECK(onFailureCalled);

        CHECK_FALSE(capturedException.empty());
        CHECK(capturedException.getMessage() != "");
    }

    STEP_then_(the_OnFinish_listener_should_be_called_with_the_state)(CallState expectedState){
        CHECK(capturedFinishState == expectedState);
    }

    STEP_then_(the_ContinueConnector_should_be_finished_with_state)(CallState expectedState){
        CHECK(junction->hasFinished());
        CHECK(junction->status() == expectedState);
    }

public:
    STEP_given_(that_the_AsyncHandle_returned_by_the_callable_is_cancellable)(){
        REQUIRE(strHandleMockup);

        strHandleMockup->cancellable([](){
            return true;
        });

        CHECK(strHandleMockup->isCancellable());
    }

    STEP_when_(the_ContinueConnector_is_cancelled)(){
        CHECK(junction->cancel());
    }

protected:
    using IntToStringContinuation = ContinueConnector<int, std::string>;
    using VoidToStringJunction = ContinueConnector<void, std::string>;

    std::shared_ptr<IntToStringContinuation> junction;
    std::shared_ptr<VoidToStringJunction> voidJunction;

    FuturableMockup<std::string>::Ptr strHandleMockup;

    int capturedResult=-1, expectedResult=0;
    std::string capturedStrResult, expectedStrResult;
    bool functionCalled=false;
    bool onSuccessCalled=false, onFailureCalled=false;

    ExceptionStorage capturedException;

    CallState capturedFinishState = CallState::Unknown;
};
