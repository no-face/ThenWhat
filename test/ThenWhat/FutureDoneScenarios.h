#pragma once

#include "test.h"

#include "ThenWhat/impl/FutureDone.h"
#include "ThenWhat/detail/Ptr.h"

using namespace ThenWhat;

class FutureDoneScenarios : public TestFixture<FutureDoneScenarios>{
public://creation
    STEP_when_(a_FutureDone_is_created_from_some_value)(){
        expectedStrResult = "Hello World";
        strFuture = std::make_shared<FutureDone<std::string>>(expectedStrResult);
    }

    STEP_when_(a_FutureDone_is_created_from_some_ExceptionStorage)(){
        expectedError = ExceptionStorage(std::runtime_error("Failed"));
        strFuture = std::make_shared<FutureDone<std::string>>(expectedError);
    }

    STEP_when_(a_non_void_FutureDone_is_default_created)(){
        strFuture = std::make_shared<FutureDone<std::string>>();
    }

    STEP_when_(a_void_FutureDone_is_default_created)(){
        voidFuture = std::make_shared<FutureDone<void>>();
    }

    STEP_then_(it_should_have_a_state_of)(CallState state, Ptr<FuturableCommon> future=nullptr){
        if(!future){
            future = this->strFuture;
        }

        REQUIRE(future);
        CHECK(future->status() == state);
    }

    STEP_then_(it_should_be_finished)(){
        CHECK(strFuture->hasFinished());
    }

public://OnSuccess listener
    STEP_given_(a_successful_FutureDone_instance)(){
        when().a_FutureDone_is_created_from_some_value();
    }

    STEP_when_(registering_an_OnSuccess_listener)(){
        REQUIRE(strFuture);

        strFuture->onSuccess([this](const std::string & result){
            this->capturedStrResult = result;
        });
    }

    STEP_then_(the_stored_value_should_be_passed_to_the_OnSuccess_listener)(){
        CHECK(capturedStrResult == expectedStrResult);
    }

public:
    STEP_given_(a_failed_FutureDone_instance)(){
        when().a_FutureDone_is_created_from_some_ExceptionStorage();
    }

    STEP_when_(registering_an_OnFailure_listener)(){
        strFuture->onFailure([this](const ExceptionStorage & error){
            this->capturedError = error;
        });
    }

    STEP_then_(the_stored_error_should_be_passed_to_the_OnFailure_listener)(){
        CHECK(capturedError);
        CHECK(capturedError == expectedError);
        CHECK(capturedError.getMessage() == expectedError.getMessage());
    }

public:
    STEP_when_(registering_an_OnFinish_listener)(){
        REQUIRE(strFuture);

        strFuture->onFinish([this](CallState state){
            this->capturedState = state;
        });
    }

    STEP_then_(the_OnFinish_listener_should_be_called_with_the_current_state)(CallState expectedState){
        CHECK(capturedState == expectedState);
    }

protected:
    Ptr<FutureDone<std::string>> strFuture;
    Ptr<FutureDone<void>> voidFuture;

    std::string expectedStrResult, capturedStrResult;
    ExceptionStorage expectedError, capturedError;
    CallState capturedState{CallState::Unknown};
};
