#pragma once

#include "test.h"

#include "ThenWhat/Future.h"
#include "ThenWhat/exceptions.h"

#include "mockups/FuturableMockup.h"

using namespace ThenWhat;

#define CAPTURE_EXCEPTION(X) \
    try{X;} \
    catch(...) {capturedExceptions.push_back(ExceptionStorage::capture());}


class FutureScenarios : public TestFixture<FutureScenarios>{
public:
    STEP_given_(a_Futurable_pointer)(){
        futurableMockup = FuturableMockup<int>::build();
    }

    STEP_when_(creating_a_Future_instance_from_it)(){
        future = {futurableMockup};
    }

    STEP_then_(it_should_store_that_Futurable_instance)(){
        CHECK(future.getFuturable() == futurableMockup);
    }

public:
    STEP_given_(a_Future_instance_created_from_some_Futurable_pointer)(){
        given().a_Futurable_pointer();
        when().creating_a_Future_instance_from_it();
    }

    STEP_when_(converting_it_into_a_Futurable_pointer)(){
        futurablePointer = future;
    }

    STEP_then_(it_should_return_the_internal_futurable)(){
        CHECK(futurablePointer == future.getFuturable());
    }

public:
    STEP_when_(calling_any_method_from_Futurable_interface)(){
        CAPTURE_EXCEPTION(
            future.onFailure([](const ExceptionStorage &){})
        );
        CAPTURE_EXCEPTION(
            future.onSuccess([](int){})
        );
        CAPTURE_EXCEPTION(
            future.onFinish([](CallState){})
        );

        CAPTURE_EXCEPTION(future.status());
        CAPTURE_EXCEPTION(future.hasFinished());
        CAPTURE_EXCEPTION(future.isCancellable());
        CAPTURE_EXCEPTION(future.cancel());
    }

    STEP_then_(the_method_should_be_delegated_to_the_Futurable)(){
        REQUIRE(futurableMockup != nullptr);

        CHECK(futurableMockup->onSuccessCalls > 0);
        CHECK(futurableMockup->onFailureCalls > 0);
        CHECK(futurableMockup->onFinishCalls > 0);

        CHECK(futurableMockup->hasFinishedCalls > 0);
        CHECK(futurableMockup->isCancellableCalls > 0);
        CHECK(futurableMockup->cancelCalls > 0);
        CHECK(futurableMockup->statusCalls > 0);
    }

public:
    STEP_given_(a_Future_instance_with_a_null_Futurable_pointer)(){
        future = {nullptr};
    }

    STEP_then_(an_NullDelegate_exception_should_be_thrown)(){
        CHECK(capturedExceptions.size() == 7);

        for(const ExceptionStorage & e : capturedExceptions){
            CHECK_THROWS_AS(e.rethrow(), NullDelegateException);
        }
    }

protected:
    Future<int> future;
    FuturableMockup<int>::Ptr futurableMockup;

    std::shared_ptr<Futurable<int>> futurablePointer;
    std::vector<ExceptionStorage> capturedExceptions;
};
