#include "ContinuationScenarios.h"

#define FIXTURE ContinuationScenarios
#define TEST_TAG "[Continuation]"

/* *********************************************************/


template<typename T1, typename T2>
bool is_same_type(){
    return std::is_same<T1, T2>::value;
}

template<typename T1, typename T2>
void checkTypeEquals(){
    CAPTURE(typeid(T1).name());
    CAPTURE(typeid(T2).name());

    CHECK((is_same_type<T1, T2>()));
}

#define TYPE_EQUALS(type1, type2) \
    checkTypeEquals<type1, type2>()

FIXTURE_SCENARIO("[continuation_traits] ... "){
    TYPE_EQUALS(int, typename future_results<Futurable<int>>::result_type);
    TYPE_EQUALS(int, typename future_results<FuturablePtr<int>>::result_type);

    using IntMockupHandlePtr = std::shared_ptr<FuturableMockup<int>>;

    TYPE_EQUALS(int, typename future_results<IntMockupHandlePtr>::result_type);


    TYPE_EQUALS(std::nullptr_t, typename future_results<std::shared_ptr<int>>::result_type);
}


FIXTURE_SCENARIO("[Continuation] can be created from an Futurable"){
    given().an_valid_Futurable_instance();
    when().creating_a_continuation_instance_from_it();
    then().the_continuation_should_have_the_same_state_of_the_futurable();
}

FIXTURE_SCENARIO("[Continuation] should delegate listeners for Futurable"){
    given().an_valid_Continuation_instance();
    when().registering_an_error_listener()
            .and_().registering_an_onFinish_listener();
    then().an_error_listener_should_be_registered_to_the_Futurable()
            .and_().an_onFinish_listener_should_be_registered_to_the_Futurable();
}

FIXTURE_SCENARIO("[Continuation] can be created from a value"){
    when().a_Continuation_is_created_from_a_compatible_value();
    then().the_Continuation_should_have_a_succeed_state();
}

FIXTURE_SCENARIO("void [Continuation] default created should be suceeded"){
    when().a_void_Continuation_is_default_created();
    then().the_void_Continuation_should_be_succeeded();
}

FIXTURE_SCENARIO("[Continuation] can chain a new Continuation from an async function"){
    given().an_valid_Continuation_instance();
    when().chaining_a_new_async_function();
    then().a_pending_Continuation_should_be_returned();
}

FIXTURE_SCENARIO("[Continuation] should register delegate listeners to the next async function"){
    given().an_valid_Continuation_instance();
    when().chaining_a_new_async_function();
    then().an_onSuccess_listener_should_be_registered_to_the_Futurable();
    and_().an_error_listener_should_be_registered_to_the_Futurable();
    and_().an_onFinish_listener_should_be_registered_to_the_Futurable();
}

FIXTURE_SCENARIO("[Continuation] should call the next function when it succeed"){
    given().a_chain_of_Continuation_instances();
    when().a_continuation_instance_receives_a_succeed_event();
    then().the_next_continuation_function_should_be_called_with_that_value();
}

FIXTURE_SCENARIO("[Continuation] chaining values"){
    given().a_started_void_Continuation();
    when().chaining_some_functions();
    then().the_returned_values_should_be_corrected_passed();
}

/* ************************* ContinuationState *************************************/

FIXTURE_SCENARIO("[Continuation] should store the async handle into a ContinuationState"){
    given().a_ContinuationState_pointer();
    and_().an_valid_Futurable_instance();
    when().creating_a_continuation_instance_from_then();
    then().the_Futurable_should_have_been_stored_at_the_ContinuationState();
}

FIXTURE_SCENARIO("[Continuation] should pass the same continuationStorage pointer to chained continuations"){
    given().a_Continuation_instance_created_from_some_ContinuationState();
    when().chaining_a_new_async_function();
    then().the_ContinuationState_should_be_passed_to_the_new_Continuation_instance();
    and_().the_new_Futurable_should_be_added_to_the_ContinuationState();
}

FIXTURE_SCENARIO("[Continuation] should remove Futurable from ContinuationState when it succeeds"){
    given().a_chain_of_Continuation_instances_created_from_some_ContinuationState();
    when().one_Continuation_completes();
    then().one_Futurable_should_be_removed_from_storage();
}

/******************************************************************************/

FIXTURE_SCENARIO("[Continuation] can interrupt operations"){
    given().a_chain_of_some_int_Continuation_instances();
    given().that_an_onFinish_listener_is_registered_to_the_last_Continuation_instance();
    when().interrupting_the_continuation();
    then().all_chained_functions_should_be_cancelled();
    and_().the_onFinish_listener_should_have_been_called_with_$_state(CallState::Cancelled);
}

FIXTURE_SCENARIO("[Continuation] should cancel all non-finished functions when interrupted"){
    given().a_chain_of_some_int_Continuation_instances();
    given().that_the_first_chained_function_has_succeeded();
    when().interrupting_the_continuation();
    then().all_functions_except_the_first_should_have_been_cancelled();
}


/******************************************************************************/

FIXTURE_SCENARIO("Continue from Futurable pointer"){
    given().an_valid_Futurable_instance();
    when().calling_continuingFrom_with_that_Futurable();
    then().a_Continuation_should_be_created_from_that_Futurable();
}

