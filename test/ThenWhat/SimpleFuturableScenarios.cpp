#include "SimpleFuturableScenarios.h"

#define FIXTURE SimpleFuturableScenarios
#define TEST_TAG "[SimpleFuturable]"

/* ****************************************************************************/

FIXTURE_SCENARIO("[SimpleFuturable] can notify when an asynchronous function succeeds"){
    given().a_SimpleAsyncHandle_instance()
            .and_().a_registered_result_listener();
    when().notifying_a_successful_result(123);
    then().the_listener_should_receive_the_result()
            .and_().the_handle_status_should_change_to(CallState::Succeed);
}

FIXTURE_SCENARIO("[SimpleFuturable] allows notify an ocurred error"){
    given().a_SimpleAsyncHandle_instance()
            .and_().a_registered_error_listener();
    when().notifying_an_error();
    then().the_listener_should_receive_the_error()
            .and_().the_handle_status_should_change_to(CallState::Failed);
}

FIXTURE_SCENARIO("[SimpleFuturable] may be cancelled if has a cancellable functor"){
    given().a_SimpleAsyncHandle_instance()
            .and_().a_registered_cancellable_functor();
    when().cancelling_the_operation();
    then().the_cancellable_should_be_called()
            .and_().the_cancel_method_should_have_returned(true)
            .and_().the_handle_status_should_change_to(CallState::Cancelled);
}

FIXTURE_SCENARIO("[SimpleFuturable] should be cancellable if it have a valid cancellable functor"){
    given().a_SimpleAsyncHandle_instance();
    when().a_cancellable_functor_is_registered();
    then().the_handle_should_be_cancellable();
}

FIXTURE_SCENARIO("[SimpleFuturable] should not be cancellable if does not have a cancellable functor"){
    given().a_SimpleAsyncHandle_instance();
    when().it_does_not_have_a_cancellable_functor();
    then().it_should_not_be_cancellable();
}

FIXTURE_SCENARIO("[SimpleFuturable] may be force cancelled if does not have cancellable"){
    given().a_SimpleAsyncHandle_instance()
            .and_().a_registered_finish_listener();
    when().it_does_not_have_a_cancellable_functor()
            .but().it_is_force_cancelled();
    then().the_cancel_method_should_have_returned(true)
            .and_().the_OnFinish_listener_should_be_called_with_status(CallState::Cancelled);
}

FIXTURE_SCENARIO("[SimpleFuturable] may be force cancelled if cancellable returns false"){
    given().a_SimpleAsyncHandle_instance()
            .and_().a_registered_cancellable_functor()
            .and_().a_registered_finish_listener();
    when().the_asyncHandle_is_force_cancelled_but_the_cancellable_fails();
    then().the_cancel_method_should_have_returned(true)
            .and_().the_OnFinish_listener_should_be_called_with_status(CallState::Cancelled);
}

/* ****************************************************************************/


FIXTURE_SCENARIO("[SimpleFuturable] should be finished after succeed"){
    given().a_SimpleAsyncHandle_instance()
            .and_().a_registered_finish_listener();
    when().notifying_a_successful_result(123);
    then().the_OnFinish_listener_should_be_called_with_status(CallState::Succeed)
            .and_().the_handle_should_be_finished();
}

FIXTURE_SCENARIO("[SimpleFuturable] should be finished after failure"){
    given().a_SimpleAsyncHandle_instance()
            .and_().a_registered_finish_listener();
    when().notifying_an_error();
    then().the_OnFinish_listener_should_be_called_with_status(CallState::Failed)
            .and_().the_handle_should_be_finished();
}

FIXTURE_SCENARIO("[SimpleFuturable] should be finished after cancel"){
    given().a_SimpleAsyncHandle_instance()
            .and_().a_registered_cancellable_functor()
            .and_().a_registered_finish_listener();
    when().cancelling_the_operation();
    then().the_OnFinish_listener_should_be_called_with_status(CallState::Cancelled)
            .and_().the_handle_should_be_finished();
}


/* ****************************************************************************/

FIXTURE_SCENARIO("[SimpleFuturable] should not notify success if already finished"){
    given().an_already_finished_asyncHandle()
            .and_().a_registered_result_listener();
    when().notifying_a_successful_result(321);
    then().the_new_result_should_not_be_propagated();
}

FIXTURE_SCENARIO("[SimpleFuturable] should not notify failure if already finished"){
    given().an_already_finished_asyncHandle()
            .and_().a_registered_error_listener();
    when().notifying_an_error();
    then().the_error_listener_should_not_be_called();
}

FIXTURE_SCENARIO("[SimpleFuturable] should not cancel if already finished"){
    given().an_already_finished_asyncHandle()
            .and_().a_registered_cancellable_functor()
            .and_().a_registered_finish_listener();
    when().cancelling_the_operation();
    then().the_handle_should_not_be_cancelled()
            .and_().the_OnFinish_listener_should_not_be_called();
}
