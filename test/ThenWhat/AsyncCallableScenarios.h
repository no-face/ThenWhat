#pragma once

#include "test.h"

#include "ThenWhat/detail/AsyncCallable.h"

#include "mockups/FuturableMockup.h"

using namespace ThenWhat;

class AsyncCallableScenarios : public TestFixture<AsyncCallableScenarios>{
public:
    STEP_given_(an_AsyncCallable_created_from_a_compatible_functor)(){
        intCallable = [this](int value){
            this->capturedValue = value;
            return 0xbeef;
        };
    }

    STEP_when_(calling_it_passing_a_compatible_value)(){
        expectedValue = 12345;
        capturedHandle = intCallable.call(expectedValue);
    }

    STEP_then_(the_functor_should_be_called_using_that_value)(){
        CHECK(capturedValue == expectedValue);
    }

    STEP_then_(a_valid_AsyncHandle_should_have_been_returned)(){
        CHECK(capturedHandle != nullptr);
    }

public:

    STEP_given_(an_AsyncCallable_created_from_a_compatible_async_functor)(){
        futurableMockup = std::make_shared<FuturableMockup<int>>();

        intCallable = [this](int v){
            capturedValue = v;
            return futurableMockup;
        };
    }

    STEP_then_(the_AsyncHandle_returned_from_the_functor_should_be_returned)(){
        CHECK(capturedHandle);
        CHECK(capturedHandle == futurableMockup);
    }

public:
    STEP_given_(an_AsyncCallable_created_from_a_void_functor)(){
        intCallable = [this](){
            functorCalled = true;
            return 1;
        };
    }

    STEP_then_(the_functor_should_have_been_called_without_a_value)(){
        CHECK(functorCalled);
    }

public:
    STEP_given_(a_void_AsyncCallable_created_from_a_compatible_functor)(){
        voidCallable = [this](int value){
            this->capturedValue = value;
        };
    }

    STEP_when_(invoking_the_void_callable_passing_a_compatible_value)(){
        expectedValue = 98765;
        capturedVoidHandle = voidCallable.call(expectedValue);
    }

public:
    STEP_given_(a_valid_AsyncCallable_with_void_arguments)(){
        emptyCallable = [this](){
            functorCalled = true;

            return FuturableMockup<void>::build();
        };
    }

    STEP_when_(invoking_the_void_callable_without_arguments)(){
        capturedVoidHandle = emptyCallable.call();

        CHECK(capturedVoidHandle);
    }

    STEP_then_(the_callable_functor_should_be_called)(){
        CHECK(functorCalled);
    }

protected:
    AsyncCallable<int(int)> intCallable;
    AsyncCallable<void(int)> voidCallable;
    AsyncCallable<void(void)> emptyCallable;

    std::shared_ptr<FuturableMockup<int>> futurableMockup;
    FuturablePtr<int> capturedHandle = nullptr;
    FuturablePtr<void> capturedVoidHandle = nullptr;

    int expectedValue = 0, capturedValue = -1;
    bool functorCalled=false;
};
