#include "AsyncCallableScenarios.h"

#define FIXTURE AsyncCallableScenarios
#define TEST_TAG "[AsyncCallable]"

/* *********************************************************/

FIXTURE_SCENARIO("[AsyncCallable] can deliver a value to a compatible function"){
    given().an_AsyncCallable_created_from_a_compatible_functor();
    when().calling_it_passing_a_compatible_value();
    then().the_functor_should_be_called_using_that_value()
            .and_().a_valid_AsyncHandle_should_have_been_returned();
}

FIXTURE_SCENARIO("[AsyncCallable] will return an Futurable pointer when called "){
    given().an_AsyncCallable_created_from_a_compatible_async_functor();
    when().calling_it_passing_a_compatible_value();
    then().the_functor_should_be_called_using_that_value()
            .and_().the_AsyncHandle_returned_from_the_functor_should_be_returned();
}

FIXTURE_SCENARIO("[AsyncCallable] support calling functions with void parameters"){
    given().an_AsyncCallable_created_from_a_void_functor();
    when().calling_it_passing_a_compatible_value();
    then().the_functor_should_have_been_called_without_a_value();
}

FIXTURE_SCENARIO("[AsyncCallable] support void return"){
    given().a_void_AsyncCallable_created_from_a_compatible_functor();
    when().invoking_the_void_callable_passing_a_compatible_value();
    then().the_functor_should_be_called_using_that_value();
}

FIXTURE_SCENARIO("[AsyncCallable] with void parameter can be called without arguments"){
    given().a_valid_AsyncCallable_with_void_arguments();
    when().invoking_the_void_callable_without_arguments();
    then().the_callable_functor_should_be_called();
}


