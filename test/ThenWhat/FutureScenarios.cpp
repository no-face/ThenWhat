#include "FutureScenarios.h"

#define FIXTURE FutureScenarios
#define TEST_TAG "[Future]"

/* ****************************************************************************/

FIXTURE_SCENARIO("[Future] can be created from a Futurable pointer"){
    given().a_Futurable_pointer();
    when().creating_a_Future_instance_from_it();
    then().it_should_store_that_Futurable_instance();
}

FIXTURE_SCENARIO("[Future] can be converted to a Futurable pointer"){
    given().a_Future_instance_created_from_some_Futurable_pointer();
    when().converting_it_into_a_Futurable_pointer();
    then().it_should_return_the_internal_futurable();
}

FIXTURE_SCENARIO("[Future] should delegate calls to a Futurable instance"){
    given().a_Future_instance_created_from_some_Futurable_pointer();
    when().calling_any_method_from_Futurable_interface();
    then().the_method_should_be_delegated_to_the_Futurable();
}

FIXTURE_SCENARIO("[Future] should throw exception when delegate is null"){
    given().a_Future_instance_with_a_null_Futurable_pointer();
    when().calling_any_method_from_Futurable_interface();
    then().an_NullDelegate_exception_should_be_thrown();
}
