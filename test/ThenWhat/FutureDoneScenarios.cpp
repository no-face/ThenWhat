#include "FutureDoneScenarios.h"

#define FIXTURE FutureDoneScenarios
#define TEST_TAG "[FutureDone]"

/* ****************************************************************************/

//Creation

FIXTURE_SCENARIO("[FutureDone] created with a result value should be successful"){
    when().a_FutureDone_is_created_from_some_value();
    then().it_should_have_a_state_of(CallState::Succeed);
    and_().it_should_be_finished();
}

FIXTURE_SCENARIO("[FutureDone] created with an ExceptionStorage should be failed"){
    when().a_FutureDone_is_created_from_some_ExceptionStorage();
    then().it_should_have_a_state_of(CallState::Failed);
    and_().it_should_be_finished();
}

FIXTURE_SCENARIO("non void [FutureDone] should have a cancelled state when default created"){
    when().a_non_void_FutureDone_is_default_created();
    then().it_should_have_a_state_of(CallState::Cancelled);
}

FIXTURE_SCENARIO("a void [FutureDone] should have a succeed state when default created"){
    when().a_void_FutureDone_is_default_created();
    then().it_should_have_a_state_of(CallState::Succeed, this->voidFuture);
}

//Listeners

FIXTURE_SCENARIO("[FutureDone] should pass the stored value to listeners if succeeded"){
    given().a_successful_FutureDone_instance();
    when().registering_an_OnSuccess_listener();
    then().the_stored_value_should_be_passed_to_the_OnSuccess_listener();
}

FIXTURE_SCENARIO("[FutureDone] should pass the stored error to listeners if failed"){
    given().a_failed_FutureDone_instance();
    when().registering_an_OnFailure_listener();
    then().the_stored_error_should_be_passed_to_the_OnFailure_listener();
}

FIXTURE_SCENARIO("[FutureDone] should pass the finished state to OnFinish listeners, when successful"){
    given().a_successful_FutureDone_instance();
    when().registering_an_OnFinish_listener();
    then().the_OnFinish_listener_should_be_called_with_the_current_state(CallState::Succeed);
}

FIXTURE_SCENARIO("[FutureDone] should pass the finished state to OnFinish listeners, when failed"){
    given().a_failed_FutureDone_instance();
    when().registering_an_OnFinish_listener();
    then().the_OnFinish_listener_should_be_called_with_the_current_state(CallState::Failed);
}

