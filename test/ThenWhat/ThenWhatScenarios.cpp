#include "ThenWhatScenarios.h"

#define FIXTURE ThenWhatScenarios
#define TEST_TAG "[ThenWhat]"

/* *********************************************************/

SCENARIO_PENDING("[ThenWhat] enable an async function to notify a result")

SCENARIO_PENDING("[ThenWhat] enable an async function to notify a failure")

SCENARIO_PENDING("[ThenWhat] enable a client to cancel an async function")

SCENARIO_PENDING("[ThenWhat] can chain async functions")
