#include "StoredHandleScenarios.h"

#define FIXTURE StoredHandleScenarios
#define TEST_TAG "[StoredFuture]"

/* ****************************************************************************/

FIXTURE_SCENARIO("[StoredFuture] should call new OnSuccess listeners if the operation already succeded"){
    given().a_succeeded_StoredHandle();
    when().a_new_OnSuccess_listener_is_registered();
    then().it_should_be_called_with_the_stored_value();
}

FIXTURE_SCENARIO("void [StoredFuture] should call new OnSuccess listeners if the operation already succeded"){
    given().a_succeeded_void_StoredHandle();
    when().a_new_compatible_OnSuccess_listener_is_registered();
    then().the_listener_should_be_called();
}

FIXTURE_SCENARIO("[StoredFuture] should call new OnError listeners if the operation already failed"){
    given().a_failed_StoredHandle();
    when().a_new_OnFailure_listener_is_registered();
    then().it_should_be_called_with_the_stored_exception();
}

FIXTURE_SCENARIO("[StoredFuture] should call new OnFinish listeners if the operation already succeeded"){
    given().a_succeeded_StoredHandle();
    when().a_new_OnFinish_listener_is_registered();
    then().it_should_be_called_with_the_handle_state();
}

FIXTURE_SCENARIO("[StoredFuture] should call new OnFinish listeners if the operation already failed"){
    given().a_failed_StoredHandle();
    when().a_new_OnFinish_listener_is_registered();
    then().it_should_be_called_with_the_handle_state();
}
