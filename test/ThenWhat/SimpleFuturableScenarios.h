#pragma once

#include "test.h"

#include "ThenWhat/impl/SimpleFuturable.h"

using namespace ThenWhat;

class SimpleFuturableScenarios : public TestFixture<SimpleFuturableScenarios>{
public:
    STEP_given_(a_SimpleAsyncHandle_instance)(){}
    STEP_given_(a_registered_result_listener)(){
        asyncHandle.onSuccess([this](int x){
            onSuccessCalled = true;
            capturedValue = x;
        });
    }
    STEP_when_(notifying_a_successful_result)(int result){
        expectedResult = result;
        asyncHandle.success(expectedResult);
    }
    STEP_then_(the_listener_should_receive_the_result)(){
        CHECK(onSuccessCalled);
        CHECK(capturedValue == expectedResult);
    }

    STEP_then_(the_handle_status_should_change_to)(CallState expectedState){
        CHECK(asyncHandle.status() == expectedState);
    }

public:
    STEP_given_(a_registered_error_listener)(){
        asyncHandle.onFailure([this](const ExceptionStorage & err){
                this->capturedError = err;
                this->onErrorCalled = true;
            });
    }

    STEP_when_(notifying_an_error)(){
        expectedError = ExceptionStorage(std::runtime_error("Example ExceptionStorage"));
        asyncHandle.failed(expectedError);
    }

    STEP_then_(the_listener_should_receive_the_error)(){
        CHECK(onErrorCalled);
        CHECK(capturedError);
        CHECK(capturedError == expectedError);
    }

public: //cancel

    STEP_given_(a_registered_cancellable_functor)(){
        asyncHandle.cancellable([this](){
            cancellableCalled=true;
            return cancellableReturn;
        });
    }

    STEP_when_(cancelling_the_operation)(){
        cancelled = asyncHandle.cancel();
    }

    STEP_when_(it_is_force_cancelled)(){
        cancelled = asyncHandle.cancel(true);
    }
    STEP_when_(the_asyncHandle_is_force_cancelled_but_the_cancellable_fails)(){
        cancellableReturn = false;

        when().it_is_force_cancelled();
    }

    STEP_then_(the_cancellable_should_be_called)(){
        CHECK(cancellableCalled);
    }

    STEP_then_(the_cancel_method_should_have_returned)(bool expectedResult){
        CHECK(cancelled == expectedResult);
    }

    // Cancellable

    STEP_ALIAS(a_cancellable_functor_is_registered,
               a_registered_cancellable_functor)

    STEP_when_(it_does_not_have_a_cancellable_functor)(){
        asyncHandle.cancellable((CallablePtr<bool>)nullptr);
    }

    STEP_then_(the_handle_should_be_cancellable)(){
        CHECK(asyncHandle.isCancellable());
    }

    STEP_then_(it_should_not_be_cancellable)(){
        CHECK_FALSE(asyncHandle.isCancellable());
    }

public: //finish
    STEP_given_(a_registered_finish_listener)(){
        asyncHandle.onFinish([this](CallState finishState){
                this->capturedState = finishState;
                this->onFinishCalled = true;
            });
    }

    STEP_then_(the_OnFinish_listener_should_be_called_with_status)(CallState expectedState){
        CHECK(onFinishCalled);
        CHECK(capturedState == expectedState);
    }

    STEP_then_(the_handle_should_be_finished)(){
        CHECK(asyncHandle.hasFinished());
    }

public: //already finished
    STEP_given_(an_already_finished_asyncHandle)(){
        given().a_SimpleAsyncHandle_instance();
        when().notifying_a_successful_result(1010);
    }

    STEP_then_(the_new_result_should_not_be_propagated)(){
        CHECK_FALSE(onSuccessCalled);
        CHECK(capturedValue != expectedResult);
    }


    STEP_then_(the_error_listener_should_not_be_called)(){
        CHECK_FALSE(onErrorCalled);
        CHECK_FALSE(capturedError);
        CHECK(capturedError != expectedError);
    }

    STEP_then_(the_handle_should_not_be_cancelled)(){
        CHECK_FALSE(cancellableCalled);
        CHECK_FALSE(cancelled);
    }

    STEP_then_(the_OnFinish_listener_should_not_be_called)(){
        CHECK_FALSE(onFinishCalled);
    }

protected:
    SimpleFuturable<int> asyncHandle;
    int capturedValue=0;
    int expectedResult=-1;
    bool onSuccessCalled=false, onErrorCalled=false, cancellableCalled=false, onFinishCalled=false;
    bool cancelled=false;
    bool cancellableReturn=true;

    ExceptionStorage capturedError, expectedError;
    CallState capturedState=CallState::Unknown;
};
