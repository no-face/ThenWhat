#include "ContinueConnectorScenarios.h"

#define FIXTURE ContinueConnectorScenarios
#define TEST_TAG "[ContinueConnector]"

/* *********************************************************/

FIXTURE_SCENARIO("[ContinueConnector] should call async function when receives a value"){
    given().an_ContinueConnector_created_from_an_AsyncCallable();
    when().it_receives_a_successful_result();
    then().the_AsyncCallable_should_be_called_with_the_received_value();
}

FIXTURE_SCENARIO("[ContinueConnector] can be started with no arguments when 'In' type is void"){
    given().a_valid_void_ContinueConnector();
    when().start_is_called();
    then().the_AsyncCallable_should_be_called();
}

FIXTURE_SCENARIO("[ContinueConnector] should notify when the async function succeeds"){
    given().a_ContinueConnector_waiting_for_the_AsyncCallable_result();
    given().that_an_OnSuccess_listener_has_been_registered();
    when().the_AsyncCallable_succeeds();
    then().the_OnSuccess_listener_should_be_called_with_the_result();
}

FIXTURE_SCENARIO("[ContinueConnector] should notify when the async function fails"){
    given().a_ContinueConnector_waiting_for_the_AsyncCallable_result();
    given().that_an_error_listener_has_been_registered();
    when().the_AsyncCallable_fails();
    then().the_error_listener_should_be_called_with_the_error_instance();
}

FIXTURE_SCENARIO("[ContinueConnector] should notify when the async function finish with success"){
    given().a_ContinueConnector_waiting_for_the_AsyncCallable_result();
    given().that_an_OnFinish_listener_has_been_registered();
    when().the_AsyncCallable_succeeds();
    then().the_OnFinish_listener_should_be_called_with_the_state(CallState::Succeed);
    and_().the_ContinueConnector_should_be_finished_with_state(CallState::Succeed);
}

FIXTURE_SCENARIO("[ContinueConnector] should notify when the async function finish with error"){
    given().a_ContinueConnector_waiting_for_the_AsyncCallable_result();
    given().that_an_OnFinish_listener_has_been_registered();
    when().the_AsyncCallable_fails();
    then().the_OnFinish_listener_should_be_called_with_the_state(CallState::Failed);
    and_().the_ContinueConnector_should_be_finished_with_state(CallState::Failed);
}

FIXTURE_SCENARIO("[ContinueConnector] should notify when the async function is cancelled"){
    given().a_ContinueConnector_waiting_for_the_AsyncCallable_result();
    given().that_an_OnFinish_listener_has_been_registered();
    when().the_AsyncCallable_is_cancelled();
    then().the_OnFinish_listener_should_be_called_with_the_state(CallState::Cancelled);
    and_().the_ContinueConnector_should_be_finished_with_state(CallState::Cancelled);
}


FIXTURE_SCENARIO("[ContinueConnector] should notify if finished externally"){
    given().a_ContinueConnector_waiting_for_the_AsyncCallable_result();
    given().that_an_OnFinish_listener_has_been_registered();
    when().the_AsyncCallable_is_finished_with_state(CallState::Cancelled);
    then().the_OnFinish_listener_should_be_called_with_the_state(CallState::Cancelled);
    and_().the_ContinueConnector_should_be_finished_with_state(CallState::Cancelled);
}

FIXTURE_SCENARIO("[ContinueConnector] can be cancelled before start"){
    given().an_ContinueConnector_created_from_an_AsyncCallable();
    given().that_an_OnFinish_listener_has_been_registered();
    when().the_ContinueConnector_is_cancelled();
    then().the_OnFinish_listener_should_be_called_with_the_state(CallState::Cancelled);
    and_().the_ContinueConnector_should_be_finished_with_state(CallState::Cancelled);
}


FIXTURE_SCENARIO("[ContinueConnector] can be cancelled after start if the delegate Futurable is cancellable"){
    given().a_ContinueConnector_waiting_for_the_AsyncCallable_result();
    given().that_the_AsyncHandle_returned_by_the_callable_is_cancellable();
    given().that_an_OnFinish_listener_has_been_registered();
    when().the_ContinueConnector_is_cancelled();
    then().the_OnFinish_listener_should_be_called_with_the_state(CallState::Cancelled);
    and_().the_ContinueConnector_should_be_finished_with_state(CallState::Cancelled);

}
