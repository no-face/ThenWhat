#pragma once

#include "test.h"

#include "ThenWhat/impl/StoredFuture.h"

using namespace ThenWhat;

class StoredHandleScenarios : public TestFixture<StoredHandleScenarios>{
public://onSuccess
    STEP_given_(a_succeeded_StoredHandle)(){
        expectedResult = 123456L;
        CHECK(storedHandle.success(expectedResult));
        CHECK(storedHandle.hasFinished());
        CHECK(storedHandle.status() == CallState::Succeed);
    }
    STEP_when_(a_new_OnSuccess_listener_is_registered)(){
        storedHandle.onSuccess([this](long result){
            this->capturedResult = result;
        });
    }
    STEP_then_(it_should_be_called_with_the_stored_value)(){
        CHECK(capturedResult == expectedResult);
    }

public://onSuccess void
    STEP_given_(a_succeeded_void_StoredHandle)(){
        CHECK(voidStoredHandle.success());

        CHECK(voidStoredHandle.status() == CallState::Succeed);
    }

    STEP_when_(a_new_compatible_OnSuccess_listener_is_registered)(){
        voidStoredHandle.onSuccess([this](){
            this->onSuccessCalled = true;
        });
    }

    STEP_then_(the_listener_should_be_called)(){
        CHECK(onSuccessCalled);
    }

public://onFailure
    STEP_given_(a_failed_StoredHandle)(){
        CHECK(storedHandle.failed(std::runtime_error("Error example")));
        CHECK(storedHandle.status() == CallState::Failed);
    }
    STEP_when_(a_new_OnFailure_listener_is_registered)(){
        storedHandle.onFailure([this](const ExceptionStorage & e){
            this->onFailureCalled = true;
            this->capturedException = e;
        });
    }
    STEP_then_(it_should_be_called_with_the_stored_exception)(){
        CHECK(capturedException);
        CHECK(onFailureCalled);
    }

public://onFinish
    STEP_when_(a_new_OnFinish_listener_is_registered)(){
        storedHandle.onFinish([this](CallState callState){
            this->onFinishCalled = true;
            this->capturedCallState = callState;
        });
    }

    STEP_then_(it_should_be_called_with_the_handle_state)(){
        CHECK(onFinishCalled);
        CHECK(capturedCallState == storedHandle.status());
    }

protected:

    StoredFuture<long> storedHandle;
    StoredFuture<void> voidStoredHandle;

    long expectedResult=-1, capturedResult=0;
    ExceptionStorage capturedException;
    bool onSuccessCalled=false, onFailureCalled=false, onFinishCalled=false;
    CallState capturedCallState=CallState::Unknown;
};
