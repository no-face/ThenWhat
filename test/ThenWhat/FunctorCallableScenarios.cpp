#include "FunctorCallableScenarios.h"

#define FIXTURE FunctorCallableScenarios
#define TEST_TAG "[FunctorCallable]"

/* ****************************************************************************/

FIXTURE_SCENARIO("[FunctorCallable] can be created from std::function"){
    given().a_functor_instance();
    when().creating_a_simple_callable()
          .and_().calling_it();
    then().the_functor_should_have_been_called();
}


FIXTURE_SCENARIO("[FunctorCallable] should throw when functor is empty"){
    given().a_valid_SimpleCallable();
    when().resetting_it()
          .and_().calling_it();
    then().an_exception_should_have_been_called()
          .and_().the_callable_should_not_be_valid_anymore();
}
