#pragma once

#include "test.h"

#include "ThenWhat/ExceptionStorage.h"

using namespace ThenWhat;

class ExceptionStorageScenarios : public TestFixture<ExceptionStorageScenarios>{
public:
    STEP_given_(an_exception_with_message)(const std::string & msg){
        exception = std::make_exception_ptr(std::runtime_error(msg));
    }

    STEP_when_(storing_that_exception)(){
        exceptionStorage = ExceptionStorage(exception);
    }

    STEP_then_(the_ExceptionStorage_message_should_be)(const std::string & expectedMessage){
        CHECK(exceptionStorage.getMessage() == expectedMessage);
    }

public:
    STEP_given_(a_non_empty_ExceptionStorage)(){
        expectedMessage = "sample";
        exceptionStorage = ExceptionStorage(std::runtime_error(expectedMessage));
    }

    STEP_when_(trying_to_catch_the_stored_exception_with_a_compatible_functor)(){
        exceptionStorage.tryCatch([this](const std::runtime_error & e){
            this->capturedException = ExceptionStorage(e);
        });
    }

    STEP_then_(the_functor_should_have_been_called_with_the_stored_exception)(){
        CHECK_FALSE(capturedException.empty());
        CHECK(capturedException.getMessage() == expectedMessage);
    }

public:
    STEP_given_(an_stored_exception)(){
        exceptionStorage = ExceptionStorage(std::invalid_argument("example exception"));
    }

    STEP_when_(chaining_captors_to_capture_that_exception)(){
        exceptionStorage.tryCatch([this](const std::domain_error & e){//incorrect type
            ++unexpectedCallCount;
        })
        .tryCatch([this](const std::invalid_argument &){//correct type
            ++expectedCallCount;
        })
        .tryCatch([this](const std::exception &){//already captured
            ++unexpectedCallCount;
        });
    }

    STEP_then_(the_first_matching_captor_should_be_called)(){
        CHECK(expectedCallCount == 1);
    }

    STEP_then_(the_other_captors_should_not_have_been_called)(){
        CHECK(unexpectedCallCount == 0);
    }

protected:

    template<typename ExceptionType, typename OtherException>
    void checkExceptionType(const OtherException & ex, bool expectedResult){
        if(expectedResult){
            CHECK(ExceptionStorage(ex).instanceOf<ExceptionType>());
        }
        else{
            CHECK_FALSE(ExceptionStorage(ex).instanceOf<ExceptionType>());
        }
    }

protected:
    ExceptionStorage exceptionStorage;

    std::exception_ptr exception;
    std::string expectedMessage;

    ExceptionStorage capturedException;

    int expectedCallCount = 0;
    int unexpectedCallCount = 0;
};
