#pragma once

#include "test.h"

#include "ThenWhat/Continuation.h"
#include "ThenWhat/continuation.h"

#include "mockups/FuturableMockup.h"

#include <memory>

using namespace ThenWhat;
using ThenWhat::detail::ContinuationState;

using ContinuationPtr = std::shared_ptr<Continuation<>>;

class ContinuationScenarios : public TestFixture<ContinuationScenarios>{
public: //basic
    STEP_given_(an_valid_Futurable_instance)(){
        intFuturableMockup = FuturableMockup<int>::build();
    }

    STEP_when_(creating_a_continuation_instance_from_it)(){
        intContinuation = Continuation<int>(intFuturableMockup);
    }

    STEP_then_(the_continuation_should_have_the_same_state_of_the_futurable)(){
        CHECK(intContinuation.status() == intFuturableMockup->status());
    }

public:
    STEP_given_(an_valid_Continuation_instance)(){
        given().an_valid_Futurable_instance();
        when().creating_a_continuation_instance_from_it();
    }

    STEP_when_(registering_an_error_listener)(){
        intContinuation.onError([](const ExceptionStorage & e){

        });
    }

    STEP_when_(registering_an_onFinish_listener)(){
        intContinuation.onFinish([](CallState finishState){

        });
    }

    STEP_then_(an_onSuccess_listener_should_be_registered_to_the_Futurable)(){
        CHECK_FALSE(intFuturableMockup->onSuccessListeners().empty());
    }

    STEP_then_(an_error_listener_should_be_registered_to_the_Futurable)(){
        CHECK_FALSE(intFuturableMockup->onFailureListeners().empty());
    }

    STEP_then_(an_onFinish_listener_should_be_registered_to_the_Futurable)(){
        CHECK_FALSE(intFuturableMockup->onFinishListeners().empty());
    }

public: //create from value
    STEP_when_(a_Continuation_is_created_from_a_compatible_value)(){
        intContinuation = Continuation<int>(123);
    }

    STEP_then_(the_Continuation_should_have_a_succeed_state)(){
        CHECK(intContinuation.status() == CallState::Succeed);
    }

public: //void default created
    STEP_when_(a_void_Continuation_is_default_created)(){
        voidContinuation = Continuation<>();
    }

    STEP_then_(the_void_Continuation_should_be_succeeded)(){
        CHECK(voidContinuation.status() == CallState::Succeed);
    }

public:
    STEP_when_(chaining_a_new_async_function)(){
        voidHandleMockup = FuturableMockup<void>::build();
        capturedVoidContinuation = intContinuation.then([this](int result){
            this->capturedValue = result;
            return voidHandleMockup;
        });
    }

    STEP_then_(a_pending_Continuation_should_be_returned)(){
        CHECK(capturedVoidContinuation.status() == CallState::Pending);
    }

public:

    STEP_given_(a_chain_of_Continuation_instances)(){
        given().an_valid_Continuation_instance();
        when().chaining_a_new_async_function();
    }

    STEP_given_(a_void_continuation_with_a_next_function)(){
        voidContinuation.then([this](){
            this->functionCalled = true;

            this->intFuturableMockup = FuturableMockup<int>::build();
            return this->intFuturableMockup;
        });
    }

    STEP_when_(a_continuation_instance_receives_a_succeed_event)(){
        REQUIRE(intFuturableMockup);

        expectedValue = 123;
        CHECK(intFuturableMockup->success(expectedValue));
    }

    STEP_then_(the_next_continuation_function_should_be_called_with_that_value)(){
        CHECK(capturedValue == expectedValue);
    }

    STEP_then_(the_next_continuation_function_should_be_called)(){
        CHECK(functionCalled);
    }

public: //chaining values
    STEP_given_(a_started_void_Continuation)(){
        when().a_void_Continuation_is_default_created();
    }

    STEP_when_(chaining_some_functions)(){
        auto captureAndIncrement = [this](int value){
            capturedValues.push_back(value);
            return makeStoredHandle<int>(value + 1);
        };

        voidContinuation.then([this](){
            return makeStoredHandle(1);
        })
        .then(captureAndIncrement) //capture 1
        .then(captureAndIncrement) //capture 2
        .then(captureAndIncrement) //capture 3
        ;

        expectedValues = {1, 2, 3};
    }

    STEP_then_(the_returned_values_should_be_corrected_passed)(){
        CHECK(expectedValues == capturedValues);
    }

public:
    STEP_given_(a_ContinuationState_pointer)(){
        continuationStorage = std::make_shared<ContinuationState>();
    }

    STEP_when_(creating_a_continuation_instance_from_then)(){
        intContinuation = Continuation<int>(intFuturableMockup, continuationStorage);
    }

    STEP_given_(a_Continuation_instance_created_from_some_ContinuationState)(){
        given().a_ContinuationState_pointer();
        and_().an_valid_Futurable_instance();
        when().creating_a_continuation_instance_from_then();
    }

    STEP_given_(a_chain_of_Continuation_instances_created_from_some_ContinuationState)(){
        given().a_Continuation_instance_created_from_some_ContinuationState();

        intContinuation.then([this](int value){
            CAPTURE(value);
            return FuturableMockup<std::string>::build();
        })
        .then([](const std::string & s){
           //do nothing
            CAPTURE(s);
            FAIL("Should not have called this");
        })
        .onFinish([](CallState){
            FAIL("Should not have finished");
        });

        this->previousStorageCount = continuationStorage->size();
    }

    STEP_when_(one_Continuation_completes)(){
        REQUIRE(intFuturableMockup);
        CHECK(intFuturableMockup->success(123));
    }

    STEP_then_(the_Futurable_should_have_been_stored_at_the_ContinuationState)(){
        CHECK(continuationStorage->size() == 1);
    }

    STEP_then_(the_ContinuationState_should_be_passed_to_the_new_Continuation_instance)(){
        CHECK(continuationStorage.use_count() > 2); //counts the pointer stored on test class and on first 'Continuation'
    }

    STEP_then_(the_new_Futurable_should_be_added_to_the_ContinuationState)(){
        CHECK(continuationStorage->size() > 1);
    }

    STEP_then_(one_Futurable_should_be_removed_from_storage)(){
        CHECK(continuationStorage->size() == (previousStorageCount - 1));
    }

public: //interrupt
    STEP_given_(a_chain_of_some_int_Continuation_instances)(){
        //initiates 'intContinuation' to be the 'head' of the continuation
        given().a_Continuation_instance_created_from_some_ContinuationState();

        chainFunctions(intContinuation, 4);
    }

    STEP_given_(that_an_onFinish_listener_is_registered_to_the_last_Continuation_instance)(){
        REQUIRE_FALSE(intContinuations.empty());

        intContinuations.back().onFinish([this](CallState callState){
            this->capturedFinishState = callState;
        });
    }

    STEP_given_(that_the_first_chained_function_has_succeeded)(){
        REQUIRE_FALSE(intContinuations.empty());

        CHECK(intFuturableMockup);
        CHECK(intFuturableMockup->success(123));

        auto secondFunctionIter = ++intContinuations.begin();

        CHECK(secondFunctionIter->status() == CallState::Pending);
    }

    STEP_when_(interrupting_the_continuation)(){
        intContinuation.interrupt();
    }

    STEP_then_(all_chained_functions_should_be_cancelled)(){
        int count = 0;
        for(const auto & f : intContinuations){
            CAPTURE(count);
            ++count;

            CHECK(f.status() == CallState::Cancelled);
        }
    }

    STEP_then_(the_onFinish_listener_should_have_been_called_with_$_state)(CallState state){
        CHECK(capturedFinishState == state);
    }

    STEP_then_(all_functions_except_the_first_should_have_been_cancelled)(){
        intContinuations.pop_front();

        then().all_chained_functions_should_be_cancelled();
    }

public: //continueFrom

    STEP_when_(calling_continuingFrom_with_that_Futurable)(){
        intContinuation = continueFrom(intFuturableMockup);
    }

    STEP_then_(a_Continuation_should_be_created_from_that_Futurable)(){
        then().the_continuation_should_have_the_same_state_of_the_futurable();

        //delegate listeners
        when().registering_an_error_listener()
                .and_().registering_an_onFinish_listener();
        then().an_error_listener_should_be_registered_to_the_Futurable()
                .and_().an_onFinish_listener_should_be_registered_to_the_Futurable();
    }

protected:
    template<typename T>
    typename FuturableMockup<T>::Ptr makeHandle(){
        return FuturableMockup<T>::build();
    }

    template<typename T>
    Ptr<StoredFuture<T>> makeStoredHandle(T value){
        auto handle = std::make_shared<StoredFuture<T>>();

        handle->success(value);

        return handle;
    }

    void chainFunctions(Continuation<int> & startContinuation, int numChained){
        chainFunctions(startContinuation, numChained, [](int){
            return FuturableMockup<int>::build();
        });
    }

    void chainFunctions(Continuation<int> & startContinuation, int numChained, AsyncCallable<int(int)> callable){
        intContinuations.clear();
        intContinuations.push_back(startContinuation);

        for (int i = 0; i < numChained; ++i) {
            //updates the last continuation
            auto continuation = intContinuations.back().then(callable.getFunctor());
            intContinuations.push_back(continuation);
        }
    }

protected:
    Continuation<int> intContinuation;
    Continuation<void> capturedVoidContinuation, voidContinuation;

    std::deque<Continuation<int>> intContinuations;

    Ptr<ContinuationState> continuationStorage;

    int capturedValue = -1, expectedValue = 0;
    bool functionCalled=false;
    std::size_t previousStorageCount = 0;
    CallState capturedFinishState;

    FuturableMockup<int>::Ptr intFuturableMockup;
    FuturableMockup<void>::Ptr voidHandleMockup;

    std::vector<int> expectedValues, capturedValues;
};
